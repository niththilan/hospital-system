<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Config;
class registerControl
{
   
    public function handle(Request $request, Closure $next)
    {
         if (session('email')!=Config::get('constants.email')) {

           return redirect('login');
        }
       
        return $next($request);
    }
}
