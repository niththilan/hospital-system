<?php

namespace App\Http\Controllers;

use App\Models\home;
use Illuminate\Http\Request;
use App\Models\ChairmenModel;
use App\Models\ServiceModel;



class HomeController extends Controller
{
    public function index()
    {
        $homes = home::all();
        $maxValue = home::count('id');
        return view('home.index', compact('homes'))
            ->with('i', (request()->input('page', 1) - 1) * 5)
            ->with('j', $maxValue);
    }
    public function create()
    {
        return view('home.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'imageUpload' => 'required',
        ]);
        $input = $request->all();
        if ($input['base64image'] || $input['base64image'] != '0') {
            // $image_type_aux = explode("image/", $image_parts[0]);
            // $image_type = $image_type_aux[1];
            $image_parts = explode(";base64,", $input['base64image']);
            $image_base64 = base64_decode($image_parts[1]);
            $folderPath = public_path('image\\');
            $filename = time() . '.'.'jpg';
            $input['image'] = "$filename";
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
        }
        home::create($input);
        return redirect()->route('home.index')
            ->with('success', 'image Added successfully.');
    }
    public function show(home $home)
    {
        return view('home.show', compact('home'));
    }
    public function edit(home $home)
    {
        return view('home.edit', compact('home'));
    }
    public function update(Request $request, home $home)
    {
        $input = $request->all();


        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        } else {
            unset($input['image']);
        }
        $home->update($input);

        return redirect()->route('home.index')
            ->with('success', 'image updated successfully');
    }
    public function destroy(home $home)
    {
        $image_path = public_path('/image') . '/' . $home->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $home->delete();
        return redirect()->route('home.index')
            ->with('success', 'image deleted successfully');
    }

    //front end
    public function fhome()
    {
        $homes = home::all();

        $Service1 = ServiceModel::paginate(4);
        $Service = ServiceModel::all();
        $ChairmenModel = ChairmenModel::all();
        return view('frontEnd.home.home')
            ->with('sliders', $homes)
            ->with('Service', $Service)
            ->with('Services', $Service1)
            ->with('messages', $ChairmenModel)
            ->with('j', 1);;
    }
    // public function footer()
    // {
    //     $Service1 = ServiceModel::paginate(5);
    //     return view('frontEnd.master.footer')
    //         ->with('Service', $Service1);

    // }
}
