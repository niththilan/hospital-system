<?php

namespace App\Http\Controllers;

use App\Models\Dashboard;
use App\Models\Patient;
use App\Models\ServiceModel;
use App\Models\DoctorModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $servicesGroup = DB::table('booking')
        ->join('doctorservice','booking.doctor_service_id','=','doctorservice.id')
        ->join('services','doctorservice.services_id','=','services.id')
        ->select(DB::raw('count(services_id) as service_count', 's_name'))
        ->groupBy('services_id')
        ->get();
// return $servicesGroup;

        $doctors = DoctorModel::count('id');
        $totalservices = ServiceModel::count('id');
        $totalappointment = Patient::count('id');
        $services = ServiceModel::select('id', 's_name')->get();
        $patienservice = Patient::all();
        // $appointments = Patient::where('service','=','baby')->count();
        return view('ddashboard.indexdashboard')
            ->with('totalappointment', $totalappointment)
            // ->with('servicesGroup', $servicesGroup)
            ->with('doctors', $doctors)
            ->with('totalservices', $totalservices)
            ->with('services', $services)
            ->with('servicesGroup',$servicesGroup);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Dashboard $dashboard)
    {
        //
    }
    public function edit(Dashboard $dashboard)
    {
        //
    }
    public function update(Request $request, Dashboard $dashboard)
    {
        //
    }
    public function destroy(Dashboard $dashboard)
    {
        //
    }
}
