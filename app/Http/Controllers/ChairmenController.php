<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\ChairmenModel;

class ChairmenController extends Controller
{
    public function chairmen()
    {
        return view('chairmen.AddChairmen');
    }
    public function addChairmen(Request $req)
    {
        $req->validate([
            'imageUpload' => 'required',
            'message' => 'required',
        ]);
        $data = new ChairmenModel;
        $input = $req->all();
        if ($input['base64image'] || $input['base64image'] != '0') {
            $folderPath = public_path('image\\chairmen\\');
            return $folderPath;
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
        }
        $data::create($input);
        return redirect('/chairmen');
    }
    public function getChairmen()
    {
        $data = ChairmenModel::all();
        return view('chairmen.ShowChairmen',['data'=>$data])
        ->with('i',0);
    }
    public function editChairmen($id)
    {
        $data = ChairmenModel::find($id);
        return view('chairmen.AddChairmen',['data'=>$data]);
    }
    public function deleteChairmen($id)
    {
        $data = ChairmenModel::find($id);
        $image_path = public_path('image\\chairmen').'\\'.$data->image;
        if( $image_path===true){
            unlink($image_path);
        }
        $data->delete();
        return redirect('/chairmen');
    }
    public function updateChairmen(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'message' => 'required',
        ]);
        $data = ChairmenModel::find($req->id);
        $image_path = public_path('image\\chairmen').'\\'.$data->image;
        if( $image_path===true){
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image\\chairmen\\');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
        }
        $data->update($input);
        return redirect('/chairmen');
    }
}
