<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DoctorModel;
use App\Models\ServiceModel;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;
use App\Models\TimeTableModel;
use App\Models\DoctorSpecialistModel;

class DoctorController extends Controller
{
    const gender = ['male', 'female', 'others'];
    const specialist=['a','b','c'];

    public function doctor()
    {
        return view('doctor.AddDoctor');
    }
    public function addDoctor(Request $req)
    {
        $req->validate([
            'name' => 'required|string',
            'imageUpload' => 'required',

            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $data = new DoctorModel;
        $input = $req->all();
        if ($input['base64image'] || $input['base64image'] != '0') {
            $folderPath = public_path('image/doctor/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }

        $data::create($input);

        return redirect('/doctor');
    }
    public function getDoctor()
    {
        $data = DoctorModel::all();
        $service = ServiceModel::select('id', 's_name')->get();
        return view('doctor.ShowDoctor', ['data' => $data, 'service' => $service])
            ->with('i', 0);
    }
    public function editDoctor($id)
    {
        $data = DoctorModel::find($id);
        $service = ServiceModel::select('id', 's_name')->get();
        return view('doctor.AddDoctor', ['data' => $data, 'service' => $service])->with('specials',self::specialist);
    }
    public function deleteDoctor($id)
    {
        $data = DoctorModel::find($id);
        $image_path = public_path('image/doctor') . '/' . $data->image;
        try {
            $data->delete();
            unlink($image_path);
        } catch (\Throwable $th) {
            return redirect('/doctor')->with('exp', 'this doctor have services');
        }
        return redirect('/doctor');
    }
    public function updateDoctor(Request $req)
    {
        $req->validate([
            'name' => 'required|string',
        ]);
        $data = DoctorModel::find($req->id);
        $image_path = public_path('image/doctor') . '/' . $data->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image/doctor/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }
        $data->update($input);
        return redirect('/doctor');
    }

    // front end

    public function showSearch()
    {
        $data_doc=DB::table('doctorservice')
        ->select('doctor_id','name')
        ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
        ->join('doctor','doctor.id','=','doctorservice.doctors_id')
        ->join('services','services.id','=','doctorservice.services_id')
        ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
        ->distinct()
        ->get();

        $data_sec=DB::table('doctorservice')
        ->select('services_id','s_name')
        ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
        ->join('doctor','doctor.id','=','doctorservice.doctors_id')
        ->join('services','services.id','=','doctorservice.services_id')
        ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
        ->distinct()
        ->get();

        return view('frontEnd.Doctor.FindDoctor')->with('gender', self::gender)->with('data_doc',$data_doc)->with('data_sec',$data_sec);
    }
    public function search(Request $req)
    {
        $q = $req->q;

        $data=DB::table('doctorservice')
        ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
        ->join('doctor','doctor.id','=','doctorservice.doctors_id')
        ->join('services','services.id','=','doctorservice.services_id')
        ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
        ->where('name', 'like', '%' . $q . '%')
        ->orWhere('s_name', 'like', '%' . $q . '%')
        ->get();

        return response()->json(['data' => $data]);
    }

    public function serviceDropDown($id)
    {
        if ($id == 0) {
            $data=DB::table('doctorservice')
            ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
            ->join('doctor','doctor.id','=','doctorservice.doctors_id')
            ->join('services','services.id','=','doctorservice.services_id')
            ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
            ->get();
            return response()->json(['data' => $data]);
        } else {
            $data=DB::table('doctorservice')
            ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
            ->join('doctor','doctor.id','=','doctorservice.doctors_id')
            ->join('services','services.id','=','doctorservice.services_id')
            ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
            ->where('services_id', '=', $id )
            ->get();

            return response()->json(['data' => $data]);
        }
    }

    public function doctorDropDown($id)
    {
        if ($id == 0) {
            $data=DB::table('doctorservice')
            ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
            ->join('doctor','doctor.id','=','doctorservice.doctors_id')
            ->join('services','services.id','=','doctorservice.services_id')
            ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
            ->get();
            return response()->json(['data' => $data]);
        } else {
            $data=DB::table('doctorservice')
            ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
            ->join('doctor','doctor.id','=','doctorservice.doctors_id')
            ->join('services','services.id','=','doctorservice.services_id')
            ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
            ->where('doctors_id', '=', $id )
            ->get();

            return response()->json(['data' => $data]);
        }
    }

    public function findAll()
    {
            $data=DB::table('doctorservice')
            ->join('doctorspecialist','doctorservice.doctors_id','=','doctorspecialist.doctor_id')
            ->join('doctor','doctor.id','=','doctorservice.doctors_id')
            ->join('services','services.id','=','doctorservice.services_id')
            ->join('time_table','time_table.doctor_service_id','=','doctorservice.id')
            ->get();

        return response()->json(['data' => $data]);
    }
}
