<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\ServiceModel;

class ServiceController extends Controller
{
    public function addServices(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            's_name' => 'required',
            's_description' => 'required',
            'l_description' => 'required'
        ]);
        $data = new ServiceModel;
        $input = $req->all();
        // return $input;
        if ($input['base64image'] || $input['base64image'] != '0') {
            $folderPath = public_path('image/services/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }
        $data::create($input);
        return redirect('/services');
    }
    public function getServices()
    {
        $data = ServiceModel::all();
        return view('services.ShowServices', ['data' => $data])
            ->with('i', 0);
    }
    public function editServices($id)
    {
        $data = ServiceModel::find($id);
        return view('services.AddServices', ['data' => $data]);
    }
    public function deleteServices($id)
    {
        $data = ServiceModel::find($id);
        $image_path = public_path('image/services') . '/' . $data->image;
        try {
            $data->delete();
            unlink($image_path);
        } catch (\Throwable $th) {
        return redirect('/services')->with('exp','doctor have service');
        }
        return redirect('/services');
    }
    public function updateServices(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            's_name' => 'required',
            's_description' => 'required',
            'l_description' => 'required'
        ]);
        $data = ServiceModel::find($req->id);
        $image_path = public_path('image/services') . '/' . $data->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image/services/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        } else {
            unset($input['image']);
        }
        $data->update($input);
        return redirect('/services');
    }


    // front End

    public function getServicesFive()
    {
        $datas=ServiceModel::all();
        return view('frontEnd.services.ShowServices')
        ->with('j',1)
        ->with('i',1)
        ->with('datas',$datas);
    }

    public function getServicesById($id)
    {
       $data=ServiceModel::find($id);
       return view('frontEnd.services.ShowService')->with('data',$data);
    }

    public function viewMore()
    {
        $datas= ServiceModel::paginate(10);
        return view('frontEnd.services.ShowServices')->with('datas',$datas)
        ->with('j',1);
    }
}
