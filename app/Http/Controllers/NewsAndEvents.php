<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\news_events;

class NewsAndEvents extends Controller
{
    public function addNewsAndEvents(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'=>'required',
            's_description'=>'required',
            'l_description'=>'required',
        ]);
        $data=new news_events;
        $input = $req->all();
        if ($input['base64image'] || $input['base64image'] != '0') {
            $folderPath = public_path('image/newsevetimage/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }
        $data::create($input);
        return redirect('/newsAndEvents');
    }
    public function getNewsAndEvents()
    {
       $data=news_events::all();
       return view('news_events.showNewsAndEvents',['data'=>$data])
       ->with('i',0);
    }
    public function editNewsAndEvents($id)
    {
        $data=news_events::find($id);
        return view('news_events.AddNewsAndEvents',['data'=>$data]);
    }
    public function deleteNewsAndEvents($id)
    {
        $data=news_events::find($id);
        $image_path = public_path('image/newsevetimage').'/'.$data->image;
        unlink($image_path);
        $data->delete();
        return redirect('/newsAndEvents');
    }
    public function updateNewsAndEvents(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'=>'required',
            's_description'=>'required',
            'l_description'=>'required',
        ]);
        $data=news_events::find($req->id);
        $image_path = public_path('image/newsevetimage').'/'.$data->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image/newsevetimage/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }else{
            unset($input['image']);
        }
        $data->update($input);
        return redirect('/newsAndEvents');
    }

    // front End
    public function getNewsAndEventsFive()
    {
        $datas=news_events::all();
        return view('frontEnd.newsAndEvents.ShowNewsAndEvents')->with('datas',$datas);
    }

    public function getNewsAndEventsById($id)
    {
       $data=news_events::find($id);
       return view('frontEnd.newsAndEvents.ShowNewsAndEvent')->with('data',$data);
    }

    public function viewMore()
    {
        $datas= news_events::paginate(10);
        return view('frontEnd.newsAndEvents.ShowNewsAndEvents')->with('datas',$datas);
    }
}
