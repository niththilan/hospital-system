<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Hash;
use App\Models\EmployeeModel;
use Illuminate\Support\Facades\Config;
class UsersController extends Controller
{
    public function users()
    {
        $employees=EmployeeModel::select('id','email','status','name')->get();
        return view('users.AddUsers',['employees'=>$employees]);
    }
    public function addUsers(Request $req)
    {
        $req->validate([
            'email' => 'required|string|email|max:255|unique:users',
            'password' => ['required','confirmed', Rules\Password::defaults()],
        ]);
        $emp_id=EmployeeModel::select("id")
        ->where("email", "=", $req->email)
        ->first();
        $user = User::create([
            'emp_id'=>$emp_id->id,
            'email' => $req->email,
            'password' => Hash::make($req->password),
        ]);
        $employee=EmployeeModel::find($emp_id->id);
        $employee->status=1;
        $employee->save();
        return redirect('/users');
    }
    public function getUsers()
    {
        $data = User::all();
        $constEmail=Config::get('constants.email');
        $employees=EmployeeModel::all();
        return view('users.ShowUser', ['data' => $data,'employees'=>$employees])->with('constEmail',$constEmail)
        ->with('i',0);
    }
    public function editUsers($id)
    {
        $data = User::find($id);
        $employees=EmployeeModel::all();
        return view('users.AddUsers', ['data' => $data,'employees'=>$employees]);
    }
    public function deleteUsers($id)
    {
        $data = User::find($id);
        $data->delete();
        $employee=EmployeeModel::find($data->emp_id);
        $employee->image=$employee->image;
        $employee->status=0;
        $employee->save();
        return redirect('/users');
    }
    public function updateUsers(Request $req)
    {
        $req->validate([
            'password' => ['required','confirmed', Rules\Password::defaults()],
        ]);
            $users=User::find($req->id);
            $users->email = $users->email;
            $users->password = Hash::make($req->password);
            $users->save();
        return redirect('/users');
    }
}
