<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeModel;
use App\Models\Role;
use Illuminate\Validation\Rules;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;

class EmployeeController extends Controller
{
    const catagary = [['id' => '1', 'catacary' => 'Board Of Derector'], ['id' => '2', 'catacary' => 'managers'], ['id' => '3', 'catacary' => 'Cominication staff']];
    const gender = ['Male', 'Female', 'Other'];
    public function employee()
    {
        // return self::catagary;
        $constEmail = Config::get('constants.email');
        $role = Role::all();
        return view('Employee.AddEmployee', ['role' => $role, 'catagarys' => self::catagary, 'gender' => self::gender])->with('constEmail', $constEmail);
    }
    public function addEmployee(Request $req)
    {
        $req->validate([
            'imageUpload' => 'required',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'mobile_number' => 'required|numeric|min:10|',
            'gender' => 'required',
            'designation_id' => 'required|numeric',
            'catagary_id' => 'required',
            'city' => 'required',
        ]);
        if ($req->password) {
            $req->validate([
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]);
        }
        $employee = new EmployeeModel;
        $input = $req->all();
        if ($input['base64image'] || $input['base64image'] != '0') {
            $folderPath = public_path('image/employee/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            // $input['image'] = "$filename";
            $employee->image = "$filename";
        }

        $employee->name = $req->name;
        $employee->email = $req->email;
        $employee->mobile_number = $req->mobile_number;
        $employee->gender = $req->gender;
        $employee->designation_id = $req->designation_id;
        $employee->city = $req->city;
        $employee->catagary_id = $req->catagary_id;
        if (!$req->password) {
            $employee->status = 0;
        } else {
            $employee->status = 1;
        }
        $employee->save();
        $getEmployee = EmployeeModel::select('id', 'email')->get();
        foreach ($getEmployee as $employee) {
            if ($employee['email'] == $req->email) {
                if ($req->password) {
                    $user = User::create([
                        'emp_id' => $employee['id'],
                        'email' => $req->email,
                        'password' => Hash::make($req->password),
                    ]);
                }
            }
        }
        return redirect('/employee');
    }
    public function getEmployee()
    {
        $data = EmployeeModel::all();
        $role = Role::all();
        $constEmail = Config::get('constants.email');
        return view('Employee.ShowEmployee', ['data' => $data, 'role' => $role])->with('constEmail', $constEmail)->with('catacarys', self::catagary)
            ->with('i', 0);
    }
    public function editEmployee($id)
    {
        $data = EmployeeModel::find($id);
        $role = Role::all();
        return view('Employee.AddEmployee', ['data' => $data, 'role' => $role, 'gender' => self::gender, 'catagarys' => self::catagary]);
    }
    public function deleteEmployee($id)
    {
        $data = EmployeeModel::find($id);
        $image_path = public_path('/image/employee') . '/' . $data->image;
            try {
                $data->delete();
                unlink($image_path);
            } catch (\Throwable $th) {
                return redirect('/employee')->with('exp', 'this employee is user');
            }

        // try {
        //     $data->delete();
        //     unlink($image_path);
        // } catch (\Illuminate\Database\QueryException $e) {
        //     // $data = EmployeeModel::all();
        //     // return view('Employee.ShowEmployee', ['data' => $data, 'role' => $role,'exp'=>'you can not delete','id'=>$id])
        //     // ->with('i',0);
        //     return redirect('/employee')->with('success', 'You can not delete because this employee is user!!');
        // }
        return redirect('/employee');
    }
    public function updateEmployee(Request $req)
    {
        $req->validate([
            'name' => 'required|string|max:255',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'mobile_number' => 'required|numeric|min:10|',
            'gender' => 'required',
            'designation_id' => 'required|numeric',
            'catagary_id' => 'required',
            'city' => 'required',
        ]);
        $data = EmployeeModel::find($req->id);
        $image_path = public_path('/image/employee') . '/' . $data->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image/employee/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }
        $data->update($input);
        return redirect('/employee');
    }

    // front end

    public function findEmployeeByCatacary()
    {
        $emp=[[],[],[]];
            for ($i=1; $i <sizeof($emp)+1; $i++) {
                $emp[$i-1] = EmployeeModel::select('image', 'name', 'catagary_id')->where('catagary_id','=',$i)->get();
            }
        return view('frontEnd.about.showAbout')->with('employees',$emp);
    }

}
