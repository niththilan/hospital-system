<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Health_promotion;

class HealthAndPromotionController extends Controller
{
    public function addHealthAndPromotion(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'=>'required',
            's_description'=>'required',
            'l_description'=>'required',
        ]);
        $data=new Health_promotion;
        $input = $req->all();
        if ($input['base64image'] || $input['base64image'] != '0') {
            $folderPath = public_path('image/healthAndPromotion/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }
        $data::create($input);
        return redirect('/healthAndPromotion');
    }
    public function getHealthAndPromotion()
    {
       $data=Health_promotion::all();
       return view('health_promotion.ShowHealthAndPromotion',['data'=>$data])
       ->with('i',0);
    }
    public function editHealthAndPromotion($id)
    {
        $data=Health_promotion::find($id);
        return view('health_promotion.AddHealthAndPromotion',['data'=>$data]);
    }
    public function deleteHealthAndPromotion($id)
    {
        $data=Health_promotion::find($id);
        $image_path = public_path('image/healthAndPromotion').'/'.$data->image;
        unlink($image_path);
        $data->delete();
        return redirect('/healthAndPromotion');
    }
    public static function updateHealthAndPromotion(Request $req)
    {
            $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'=>'required',
            's_description'=>'required',
            'l_description'=>'required',
        ]);
        $data=Health_promotion::find($req->id);
        $image_path = public_path('image/healthAndPromotion').'/'.$data->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image/healthAndPromotion/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }else{
            unset($input['image']);
        }
        $data->update($input);
        return redirect('/healthAndPromotion');
    }
}
