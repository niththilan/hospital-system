<?php

namespace App\Http\Controllers;

use App\Models\DoctorModel;
use Illuminate\Http\Request;
use App\Models\Patient;
use App\Models\ServiceModel;
use App\Models\BookingModel;
use App\Models\DoctorServicesModel;

class Booking extends Controller
{
    public function booking()
    {
        $patients=Patient::select('id','firstname')->get();
        $doctorServices=DoctorServicesModel::all();
        $services=ServiceModel::select('id','s_name')->get();
        $doctors=DoctorModel::select('id','name')->get();
        return view('booking.AddBooking')->with('patients',$patients)->with('doctors',$doctors)->with('services',$services)->with('doctor_Services',$doctorServices);
    }
    public function addBooking(Request $req)
    {

        $req->validate([
            'patient_id' => 'required',
            'doctor_service_id' => 'required',
            'date'=>'required'
        ]);

        $data = new BookingModel;
        $input = $req->all();
        $data::create($input);
        return redirect('/getBooking');
    }
    public function showBooking()
    {
        $patients=Patient::select('id','firstname')->get();
        $booking=BookingModel::all();  
        $doctors = DoctorModel::select('id','name')->get();
        $services = ServiceModel::select('id','s_name')->get();
        $doctorServices=DoctorServicesModel::all();

        return view('booking.ShowBooking')->with('patients',$patients)->with('booking',$booking)->with('i',0)->with('doctors',$doctors)->with('services',$services)->with('doctorServices',$doctorServices);
    }
    public function editBooking($id)
    {
        $patients=Patient::select('id','firstname')->get();
        $doctorServices=DoctorServicesModel::all();
        $data = BookingModel::find($id);
        $services=ServiceModel::select('id','s_name')->get();
        $doctors=DoctorModel::select('id','name')->get();

        return view('booking.AddBooking')->with('patients',$patients)->with('doctors',$doctors)->with('services',$services)->with('doctor_Services',$doctorServices)->with('data',$data);
    }
    public function deleteBooking($id)
    {
        $data = BookingModel::find($id);
        $data->delete();
        return redirect('/getBooking');
    }
    public function updateBooking(Request $req)
    {
        $req->validate([
            'patient_id' => 'required',
            'doctor_service_id' => 'required',
            'date'=>'required'
        ]);

        $data = BookingModel::find($req->id);
        $input = $req->all();
        $data->update($input);
        return redirect('/getBooking');
    }
}
