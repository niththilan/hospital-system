<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\TimeTableModel;
use App\Models\DoctorModel;
use App\Models\ServiceModel;
use App\Models\DoctorServicesModel;

class TimetableController extends Controller
{
    const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

      public function timeTable()
    {
        $doctorService=DoctorServicesModel::all();
        $services=ServiceModel::select('id','s_name')->get();
        $doctors=DoctorModel::select('id','name')->get();
        return view('time_table.AddTimeTable', [ 'days' => self::days,])->with('doctor_services',$doctorService)->with('services',$services)->with('doctors',$doctors);
    }
    public function addTimeTable(Request $req)
    {
        $req->validate([
            'doctor_service_id' => 'required|numeric|unique:time_table,doctor_service_id',
        ]);
        
        $data = new TimeTableModel;
        $data->doctor_service_id = $req->doctor_service_id;
        foreach (self::days as $day) {
            $start = 'start_' . $day;
            $end = 'end_' . $day;
            $timeArray = array('start'=>$req->$start,'end'=>$req->$end);
            if ($req->$start != '') {
                $timeConvert = implode('_to_', $timeArray);
                $data->$day=$timeConvert;
            }
        }
        $data->save();
        return redirect('/timeTable');
    }
    public function getTimeTable()
    {
        $data = TimeTableModel::all();
        $doctors = DoctorModel::select('id','name')->get();
        $services = ServiceModel::select('id','s_name')->get();
        $doctorService=DoctorServicesModel::all();
        return view('time_table.ShowTimeTable', ['data' => $data, 'doctors' => $doctors, 'services' => $services])->with('doctorServices',$doctorService)
            ->with('i', 0);
    }
    public function editTimeTable($id)
    {
        $data = TimeTableModel::find($id);
        $doctorServices=DoctorServicesModel::all();
        $doctors = DoctorModel::select('id','name')->get();
        $services = ServiceModel::select('id','s_name')->get();
        return view('time_table.AddTimeTable', ['data' => $data, 'doctors' => $doctors, 'days' => self::days, 'services' => $services])->with('doctor_services',$doctorServices);
    }
    public function deleteTimeTable($id)
    {
        $data = TimeTableModel::find($id);
        $data->delete();
        return redirect('/timeTable');
    }
     public function updateTimeTable(Request $req)
    {
        $req->validate([
            'doctor_service_id' => 'required|numeric',
        ]);
        $data = TimeTableModel::find($req->id);
        $data->id = $req->id;
        $data->doctor_service_id = $req->doctor_service_id;
        foreach (self::days as $day) {
            $start = 'start_' . $day;
            $end = 'end_' . $day;
            $timeArray = array('starttime' => $req->$start, 'endtime' => $req->$end);
            if ($req->$start != '') {
                $timeConvert = implode('_to_', $timeArray);
                $data->$day = $timeConvert;
            }
        }
        $data->save();
        return redirect('/timeTable');
    }
}
