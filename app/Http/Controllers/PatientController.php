<?php
namespace App\Http\Controllers;

use App\Models\BookingModel;
use App\Models\Patient;
use App\Models\DoctorModel;
use Illuminate\Http\Request;
use App\Models\ServiceModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\TimeTableModel;

class PatientController extends Controller
{

    const gender = ['male', 'female', 'others'];
         
    public function index()
    {
        $Patients = Patient::all();
        // return $Patients;
        return view('patient.indexpatient')
            ->with('Patients', $Patients = $Patients->reverse())
            ->with('i', Patient::count('id'));
    }
    public function create()
    {
        return view('patient.createpatient')->with('gender', self::gender);
    }
    public function store(Request $request)
    {
        $input = $request->all();

        $request->validate([
            'firstname' => 'required',
            'phonenumber' => 'required|numeric|min:10|',
            'gender' => 'required',
            'age'=>'required'
        ]);
        $patient=new Patient;
        $patient->firstname=$request->firstname;
        $patient->lastname=$request->lastname;
        $patient->email=$request->email;
        $patient->phonenumber=$request->phonenumber;
        $patient->gender=$request->gender;
        $patient->age=$request->age;
        $patient->save();
   
        return redirect()->route('patient.index')->with('success', 'image Added successfully.');
    }
    
    public function storeByPatient(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'phonenumber' => 'required|numeric|min:10|',
            'gender' => 'required',
            'age'=>'required|numeric',
        ]);

        if ($validator->fails()) {    
            return response()->json([   
                'status' => 400,
                'message' => 'added not successfully',
            ]);
        };
        
        $input = $request->all();
        Patient::create($input);
        
        $getPatients = Patient::all();
        //    return $getPatients;
           foreach ($getPatients as $getPatient) {
               if ($getPatient['firstname'] == $request->firstname && $getPatient['lastname'] == $request->lastname && $getPatient['phonenumber'] == $request->phonenumber) {
                       $user = BookingModel::create([
                           'patient_id' => $getPatient['id'],
                           'doctor_service_id' => $request->doctor_service_id,
                           'date' => $request->date,
                       ]);
                   }
               }
           
        return response()->json([   
            'status' => 200,
            'message' => 'added successfully',
        ]);
    }
    public function show(Patient $patient)
    {
        //
    }
    public function edit(Patient $patient)
    {
        //
    }
    public function update(Request $request, Patient $patient)
    {
        //
    }
    public function destroy(Patient $patient)
    {
       try {
        $patient->delete();
       } catch (\Throwable $th) {
        return redirect()->route('patient.index')->with('exp','this patient has doctor appoiment');
       }
        return redirect()->route('patient.index');
    }
}
