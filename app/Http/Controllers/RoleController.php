<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Validator;
class RoleController extends Controller
{
    public function addRole(Request $req)
    {
        $req->validate([
            'designation'=>'required|string|unique:role'
        ]);
        $data=new Role;
        $data->designation=$req->designation;
        $data->save();
        return redirect('/role');
    }
    public function getRole()
    {
       $data=Role::all();
       return view('role.Show_Role',['data'=>$data])
       ->with('i',0);
    }
    public function editRole($id)
    {
        $data=Role::find($id);
        return view('role.Add_Role',['data'=>$data]);
    }
    public function deleteRole($id)
    {
        $data=Role::find($id);
        try {
            $data->delete();
        } catch (\Throwable $th) {
            return redirect('/role')->with('success', 'Employees have this designation!!');
        }
        return redirect('/role');
    }
    public function updateRole(Request $req)
    {
        $req->validate([
            'designation'=>'required|string'
        ]);
        $data=Role::find($req->id);
        $data->designation=$req->designation;
        $data->save();
        return redirect('/role');
    }

    // USING AJEX

    // public function addRole(Request $req)
    //     {
    //         $validator = Validator::make($req->all(), [
    //             'designation' => 'required|string|unique:role'
    //         ]);
    
    //         if ($validator->fails()) {
    //             return response()->json([
    //                 'status' => 400,
    //             ]);
    //         } else {
    
    //             if (!$req->id) {
    //                 $data = new Role;
    //                 $data->designation = $req->designation;
    //                 $data->save();
    //             } else {
    //                 $data = Role::find($req->id);
    //                 $data->designation = $req->designation;
    //                 $data->save();
    //             }
    
    //             return response()->json([
    //                 'status' => 200,
    //                 'message' => 'added successfully',
    //             ]);
    //         }
    //     }

    //     public function getRole()
    //     {
    //         $data=Role::all();
    //         return json_encode($data);
    //     }
    //     public function index()
    //     {
    //         return view('role.Show_Role');
    //     }
    //     public function deleteRole($id)
    //     {
    //         $data = Role::find($id);
    //         $data->delete();
    //         return redirect('/role');
    //     }
}