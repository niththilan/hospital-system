<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DoctorServicesModel;
use App\Models\DoctorModel;
use App\Models\ServiceModel;

class DoctorServices extends Controller
{
    public function doctorServices()
    {
        $doctor = DoctorModel::select('id', 'name')->get();
        $service = ServiceModel::select('id', 's_name')->get();
        return view('doctor_services.AddDoctorServices')->with('doctors', $doctor)->with('services', $service);
    }
    public function addDoctorServices(Request $req)
    {
        $req->validate([
            'doctors_id' => 'required',
            'services_id' => 'required',
        ]);

        $data = new DoctorServicesModel;
        $input = $req->all();
        try {
            $data::create($input);
        } catch (\Throwable $th) {
            return redirect('/doctorServices')->with('exp','doctor already have this service');
        }
        return redirect('/doctorServices');
    }
    public function getDoctorServices()
    {
        $doctorService = DoctorServicesModel::all();
        $doctor = DoctorModel::select('id', 'name')->get();
        $service = ServiceModel::select('id', 's_name')->get();
        return view('doctor_services.ShowDoctorServices')->with('doctorServices', $doctorService)->with('doctors', $doctor)->with('services', $service)
            ->with('i', 0);
    }
    public function editDoctorServices($id)
    {
        $data = DoctorServicesModel::find($id);
        $doctor = DoctorModel::select('id', 'name')->get();
        $service = ServiceModel::select('id', 's_name')->get();
        return view('doctor_services.AddDoctorServices', ['data' => $data])->with('doctors', $doctor)->with('services', $service);
    }
    public function deleteDoctorServices($id)
    {
        $data = DoctorServicesModel::find($id);

        try {
            $data->delete();
        } catch (\Throwable $th) {
            return redirect('/doctorServices')->with('exp','doctor have service');

        }
        return redirect('/doctorServices');
    }
    public function updateDoctorServices(Request $req)
    {
        $req->validate([
            'doctors_id' => 'required',
            'services_id' => 'required',
        ]);
        $data = DoctorServicesModel::find($req->id);
        $input = $req->all();
        try {
            $data->update($input);
        } catch (\Throwable $th) {
            return redirect('/doctorServices')->with('exp','doctor already have this service');
        }
        return redirect('/doctorServices');
    }
}
