<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DoctorSpecialistModel;
use App\Models\DoctorModel;

class DoctorSpecialist extends Controller
{
    const specialist = ['a', 'b', 'c'];
    public function doctorSpecialist()
    {
        $doctor = DoctorModel::select('id', 'name')->get();
        return view('doctor_specialist.AddDoctorSpecialist')->with('doctors', $doctor)->with('specials', self::specialist);
    }
    public function addDoctorSpecialist(Request $req)
    {
        $req->validate([
            'doctor_id' => 'required|unique:doctorspecialist,doctor_id',
            'secialist' => 'required',
        ]);

        $SpecilistArray = $req['secialist'];
        $SpecilistConvert = implode(',', $SpecilistArray);

        $data = new DoctorSpecialistModel;
        $input = $req->all();
        $input['secialist'] = $SpecilistConvert;
        $data::create($input);
        return redirect('/doctorSpecialist');
    }
    public function getDoctorSpecialist()
    {
        $doctorSpecialists = DoctorSpecialistModel::all();
        $doctor = DoctorModel::select('id', 'name')->get();
        return view('doctor_specialist.ShowDoctorSpecialist', ['doctorSpecialists' => $doctorSpecialists])->with('doctors', $doctor)
            ->with('i', 0);
    }
    public function editDoctorSpecialist($id)
    {
        $data = DoctorSpecialistModel::find($id);
        $doctor = DoctorModel::select('id', 'name')->get();
        return view('doctor_specialist.AddDoctorSpecialist')->with('data', $data)->with('doctors', $doctor)->with('specials', self::specialist);
    }
    public function deleteDoctorSpecialist($id)
    {
        $data = DoctorSpecialistModel::find($id);
        $data->delete();
        return redirect('/doctorSpecialist');
    }
    public function updateDoctorSpecialist(Request $req)
    {
        $req->validate([
            'doctor_id' => 'required',
            'secialist' => 'required',
        ]);

        $SpecilistArray = $req['secialist'];
        $SpecilistConvert = implode(',', $SpecilistArray);
        $input = $req->all();
        $input['secialist'] = $SpecilistConvert;

        $data = DoctorSpecialistModel::find($req->id);
        $data->update($input);
        return redirect('/doctorSpecialist');
    }
}
