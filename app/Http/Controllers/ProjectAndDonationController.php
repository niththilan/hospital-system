<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Project_Donation;
class ProjectAndDonationController extends Controller
{
    public function addProjectAndDonation(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'=>'required|string',
            'donation'=>'required|numeric'
        ]);
        // return $req->file('image');

        $data=new Project_Donation;
        $input=$req->all();
        if ($input['base64image'] || $input['base64image'] !== '0') {
            $folderPath = public_path('image/projectAndDonation/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }

        $data::create($input);
        return redirect('/projectAndDonation');
    }
    public function getaddProjectAndDonation()
    {
        $maxValue = Project_Donation::count('id');
        $data=Project_Donation::all();
        return view('project_donation.showProjectAndDonation',['data'=>$data=$data->reverse()])
        ->with('i',$maxValue);
    }
    public function editProjectAndDonation($id)
    {
        $data=Project_Donation::find($id);
        return view('project_donation.AddProjectAndDonation',['data'=>$data]);
    }
    public function deleteProjectAndDonation($id)
    {
        $data=Project_Donation::find($id);
        $data->delete();
        return redirect('/projectAndDonation');
    }
    public function updateProjectAndDonation(Request $req)
    {
        $req->validate([
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'=>'required|string',
            'donation'=>'required|numeric|min:0|'
        ]);
        $data=Project_Donation::find($req->id);
        $image_path = public_path('image/projectAndDonation').'/'.$data->image;
        if ($image_path === true) {
            unlink($image_path);
        }
        $input = $req->all();
        if ($input['base64image'] && $input['base64image'] != '0') {
            $folderPath = public_path('image/projectAndDonation/');
            $image_parts = explode(";base64,", $input['base64image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $filename = time() . '.'. $image_type;
            $file =$folderPath.$filename;
            file_put_contents($file, $image_base64);
            $input['image'] = "$filename";
            // $employee->image = "$filename";
        }else{
            unset($input['image']);
        }
        $data->update($input);
        return redirect('/projectAndDonation');
    }
    public function addDonation(Request $req)
    {
        $req->validate([
            'donation'=>'required|numeric|min:0|'
        ]);
            $data= Project_Donation::find($req->id);
            $data->donation=$data->donation+$req->donation;
            $data->save();
        return redirect('/projectAndDonation');
    }
    public function changeStatus($id)
    {
        $data= Project_Donation::find($id);
        $data->status='no';
        $data->save();
        return redirect('/projectAndDonation');
    }

      // front End
      public function getProjectAndDonationFive()
      {
          $datas=Project_Donation::all();
          return view('frontEnd.projectAndDonations.ShowProjectsAndDonations')->with('datas',$datas);
      }

      public function getProjectAndDonationById($id)
      {
         $data=Project_Donation::find($id);
         return view('frontEnd.projectAndDonations.ShowProjectAndDonation')->with('data',$data);
      }

      public function viewMore()
      {
          $datas= Project_Donation::paginate(10);
          return view('frontEnd.projectAndDonations.ShowProjectsAndDonations')->with('datas',$datas);
      }


}
