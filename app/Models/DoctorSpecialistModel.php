<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorSpecialistModel extends Model
{
    use HasFactory;
    public $table='doctorspecialist';
    public $timestamps=false;
    public $fillable=[
        'doctor_id',
        'secialist'
    ];
}
