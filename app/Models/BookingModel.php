<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
class BookingModel extends Model
{
    use HasFactory;
    public $table='booking';
    public $fillable=[
        'patient_id',
        'doctor_service_id',
        'date',
        'created_at',
        'updated_at'
    ];
}
