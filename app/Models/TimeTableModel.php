<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeTableModel extends Model
{
    use HasFactory;
    public $table='time_table';
}
