<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    use HasFactory;
    public $table = 'employee';

    protected $fillable = [
        'name',
        'email',
        'image',
        'mobile_number',
        'gender',
        'designation_id',
        'city',
        'catagary_id'
    ];
}
