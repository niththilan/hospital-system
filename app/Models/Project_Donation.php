<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project_Donation extends Model
{
    use HasFactory;
    public $table='project_donation';
    public $timestamps=false;
    public $fillable=[
        'image',
        'description',
        'donation',
        's_detail',
        'l_detail',
        'status'
    ];
}
