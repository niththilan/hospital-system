<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutModel extends Model
{
    use HasFactory;
    public $table='about';
    public $timestamps=false;
    public $fillable=[
        'emp_id',
        'status'
    ];
}
