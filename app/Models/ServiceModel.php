<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model
{
    use HasFactory;
    public $table='services';
    protected $fillable = [
        'image',
        's_name',
        's_description',
        'l_description'
   ];
//    protected $primaryKey = 's_description';
    public $timestamps=false;
}
