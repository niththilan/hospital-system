<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Health_promotion extends Model
{
    use HasFactory;
    public $table='health_promotion';
    protected $fillable = [
        'image',
        'name',
        's_description',
        'l_description'
   ];
    public $timestamps = false;
    
}
