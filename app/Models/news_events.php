<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class news_events extends Model
{
    use HasFactory;
    public $table='news_events';
    protected $fillable = [
        'image',
        'name',
        's_description',
        'l_description'
   ];
    public $timestamps = false;
}
