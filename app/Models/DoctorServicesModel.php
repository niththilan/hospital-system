<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorServicesModel extends Model
{
    use HasFactory;
    public $table='doctorservice';
    public $timestamps=false;
    public $fillable=[
        'doctors_id',
        'services_id',
    ];
}
