<?php

namespace App\Providers;

use App\Models\ServiceModel;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontEnd.master.footer', function ($view) {

            $view->with('Service',ServiceModel::paginate(5));
        });
    }
}
