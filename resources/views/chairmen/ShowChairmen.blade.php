@extends('master.layout')

@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper  ml-3 mr-3">
        <div class="page-title-heading">
            <div>Chairmen
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block ">

                <button type="button" class="btn-shadow      btn btn-info"
                    onclick="window.location.href='/addChairmen'">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="icon ion-android-add-circle"></i>
                    </span>
                    Add
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-12 mt-3">
        <div class="main-card mb-3 card">
            <div class="card-body table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">image</th>
                            <th scope="col">message</th>
                            <th scope="col" style="width: 80px">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                {{-- <td>{{ $item['id'] }}</td> --}}
                                <td>{{++$i}}</td>
                                <td><img src="/image/chairmen/{{ $item->image }}" alt="" style="height: 80px"></td>
                                <td>{{ $item['message'] }}</td>
                                {{-- <td>{{ $item['emp_id'] }}</td> --}}

                                <td>

                                    <i onclick="window.location.href='/chairmen/{{ $item['id'] }}'" class="bi bi-pencil-square"  style="color:rgb(0, 8, 240); font-size:20px"></i>
                                    <i onclick="window.location.href='/deleteChairmen/{{ $item['id'] }}'" class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"></i>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
