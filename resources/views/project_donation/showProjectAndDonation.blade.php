@extends('master.layout')
<!-- Modal -->

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <div>project and donation
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">

                    <button type="button" class="btn-shadow      btn btn-info"
                        onclick="window.location.href='/addProjectAndDonation'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                </div>
            </div>
        </div>

        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Image</th>
                                <th scope="col">Project name</th>
                                <th scope="col" style="width: 90px">Short detail</th>
                                <th scope="col" >Long detail</th>
                                <th scope="col" style="width: 90px">donation</th>
                                <th scope="col" style="width: 200px">donate</th>
                                <th scope="col" style="width: 120px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr disabled>
                                    {{-- <td>{{ $item['id'] }}</td> --}}
                                    <td>{{ $i-- }}</td>
                                <td><img src="/image/projectAndDonation/{{ $item->image }}" alt="" style="height: 80px"></td>
                                    <td>{{ $item['description'] }}</td>
                                    <td>{{ Str::limit($item->s_detail, 80, $end = '...') }}</td>
                                    <td>{{Str::limit($item->l_detail, 100, $end='......')}}</td>
                                    <td>{{ $item['donation'] }}</td>
                                    <td>
                                        @if ($item['status'] == 'no')
                                            <h6 class="text-danger">Session Closed</h6>
                                        @else
                                            <form action="/addProjectAndDonation" method="POST" style=" margin-top: 10px"
                                                id="your_form_id">
                                                @csrf
                                                {{ method_field('PATCH') }}
                                                <div class="input-group mb-3">
                                                    <input type="hidden" name='id' value="{{ $item['id'] }}"
                                                        class="form-control">
                                                    <input type="number" step="0.0001" name='donation' class="form-control"
                                                        placeholder="donation" aria-label=""
                                                        aria-describedby="button-addon2" style="width: 10px" autocomplete="off">
                                                    <button class="btn btn-success" type="submit"
                                                        id="button-addon2">Donate</button>
                                                </div>
                                            </form>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item['status'] == 'no')
                                            <h6 class="text-danger">Closed</h6>
                                        @else
                                            <i onclick="window.location.href='/projectAndDonation/{{ $item['id'] }}'"
                                                class="bi bi-pencil-square"
                                                style="color:rgb(0, 8, 240); font-size:20px"></i>
                                            <i class="bi bi-trash-fill ml-2 delete1" style="color:red; font-size:20px"
                                                item-id ={{ $item['id'] }}  ></i>
                                            <i class="bi bi-x-circle ml-2 cancal1" style="color:rgb(153, 0, 255); font-size:20px"
                                                item-id ={{ $item['id'] }}  ></i>
                                        @endif
                                        {{-- <input type="  checkbox" checked/> --}}


                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
<div class="modal fade" id="close2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="exampleModalLabel">Are you sure want close session</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                session will close you can't make any changes
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger"
                id="yes_cancal" >yes</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="delete2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="exampleModalLabel">Are you sure want delete session</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                session will delete
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger"
                     id="yes_delete"  >yes</button>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

    $(document).ready(function(){
        // console.log(1234);
        $(".delete1").click(function(){
                var id = $(this).attr('item-id');
                console.log('delete');
                console.log(id);
                $("#delete2").modal('show');

                $("#yes_delete").click(function(){
                    window.location.href='/deleteProjectAndDonation/'+id
                });
        });

        $(".cancal1").click(function(){
                var id = $(this).attr('item-id');
                console.log('cancal1');
                console.log(id);
                $("#close2").modal('show');

                $("#yes_cancal").click(function(){
                    window.location.href='/status/'+id
                });
        });

    });
</script>
