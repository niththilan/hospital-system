@extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <div>
                    <h1 style="color: green">
                        @if (isset($data->id))
                            {{ 'Update Promotion and Donation' }}@else{{ 'Add Promotion and Donation' }}
                        @endif
                    </h1>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">

                    <button type="button" class="btn-shadow      btn btn-info" onclick="window.location.href='/projectAndDonation'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        back
                    </button>
                </div>
            </div>
        </div>

        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/projectAndDonation" method="POST" class="container" enctype="multipart/form-data">
                        @csrf
                        @if (isset($data->id)){{ method_field('PATCH') }}@endif
                        <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                        <div class="mb-3">
                            <label for="formGroupExampleInput" class="form-label">Add Image</label>
                            <div class="avatar-edit">
                                <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" name="imageUpload"
                                    class="form-control imageUpload" />
                                <input type="hidden" name="base64image" name="base64image" id="base64image">
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview container2 mt-3">

                                <div id="imagePreview">
                                </div>
                            </div>
                            <span style="color: red">
                                @error('image'){{ $message }}<br>
                                @enderror
                            </span>
                        </div>
                        <div class="mb-3">
                            <label for="formGroupExampleInput" class="form-label">Project</label>

                            <input type="text" name="description" class="form-control" id="formGroupExampleInput"
                                placeholder="Add Project" value="@if (isset($data->description)) {{ $data->description }}@else{{ old('description') }} @endif"
                                autocomplete="off">
                            <span style="color: red">
                                @error('description'){{ $message }}<br>
                                @enderror
                            </span>
                        </div>
                        <div class="mb-3">
                            <label for="formGroupExampleInput11" class="form-label">Project sort detail</label>
                            <textarea type="text" name="s_detail" class="form-control" id="formGroupExampleInput11"
                                placeholder="Add Project" value=""
                                autocomplete="off">@if (isset($data->s_detail)) {{ $data->s_detail }}@else{{ old('s_detail') }} @endif</textarea>
                            <span style="color: red">
                                @error('description'){{ $message }}<br>
                                @enderror
                            </span>
                        </div>
                        <div class="mb-3">
                            <label for="formGroupExampleInput11" class="form-label">Project detail</label>

                            <textarea type="text" name="l_detail" class="form-control" id="formGroupExampleInput11"
                                placeholder="Add Project" value=""
                                autocomplete="off">@if (isset($data->l_detail)) {{ $data->l_detail }}@else{{ old('l_detail') }} @endif</textarea>
                            <span style="color: red">
                                @error('description'){{ $message }}<br>
                                @enderror
                            </span>
                        </div>
                        <div class="mb-3">
                            <label for="formGroupExampleInput2" class="form-label">Add Donation </label>
                            <input type="text" name="donation" class="form-control" id="formGroupExampleInput"
                                placeholder="Add Donation" value="@if (isset($data->donation)) {{ $data->donation }}@else{{ old('donation') }} @endif"
                                autocomplete="off">
                            <span style="color: red">
                                @error('donation'){{ $message }}<br>
                                @enderror
                            </span>
                        </div>
                        <div class="d-block text-right card-footer ">
                            <button type="submit" class="btn btn-primary active mt-2 ">
                                @if (isset($data->id))
                                    {{ 'update' }}@else{{ 'add' }}
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="modal fade bd-example-modal-lg imagecrop" id="model" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-11">
                            <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary crop" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script>
    var $modal = $('.imagecrop');
    var image = document.getElementById('image');
    var cropper;
    $("body").on("change", ".imageUpload", function(e) {
        var files = e.target.files;
        var done = function(url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });
    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            aspectRatio: 1.5,
            viewMode: 1,
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });
    $("body").on("click", "#crop", function() {
        canvas = cropper.getCroppedCanvas({
            // width: 140,
            // height: 140,
        });
        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                $('#base64image').val(base64data);
                document.getElementById('imagePreview').style.backgroundImage = "url(" +
                    base64data + ")";
                $modal.modal('hide');
            }
        });
    })
</script>
