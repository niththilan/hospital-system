@extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id)){{ 'Update Doctor Services' }}@else{{ 'Add Doctor Services' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/doctorServices'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back"></i>
                        </span>
                        Back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/doctorServices" method="POST" class="container">
                    @csrf
                    @if (isset($data->id)){{ method_field('PATCH') }}@endif
                    <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="formGroupExampleInput" class="form-label">Add Doctor</label>
                            <select name="doctors_id" id="cars" class="form-select form-control" required>
                                <option value='' style="display: none">Choose Doctor</option>
                                @foreach ($doctors as $doctor)
                                    <option value="{{ $doctor['id'] }}" @if (isset($data->id)) {{ $doctor['id'] == $data['doctors_id'] ? 'selected' : '' }} @endif>
                                        {{ $doctor['name']}}</option>
                                @endforeach
                                <span style="color: red">
                                    @error('doctors_id'){{ $message }}<br>
                                    @enderror
                                </span>
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="formGroupExampleInput" class="form-label">Add Services</label>
                            <select name="services_id" id="cars" class="form-select form-control" required>
                                <option value='' style="display: none">Choose Services</option>
                                @foreach ($services as $service)
                                    <option value="{{ $service['id'] }}" @if (isset($data->id)) {{ $service['id'] == $data['services_id'] ? 'selected' : '' }} @endif>
                                        {{ $service['s_name']}}</option>
                                @endforeach
                                <span style="color: red">
                                    @error('services_id'){{ $message }}<br>
                                    @enderror
                                </span>
                            </select>
                        </div>
                    </div>
                    <div class="d-block text-right card-footer ">
                        <button type="submit" class="btn btn-primary active add-update">
                            @if (isset($data->id))
                                {{ 'update' }}@else{{ 'add' }}
                            @endif
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection