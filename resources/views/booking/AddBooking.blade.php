
@extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id))
                        {{ 'Update Booking' }}@else{{ 'Add Booking' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/getBooking'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back"></i>
                        </span>
                        Back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/booking" method="POST" class="container">
                        @csrf
                        @if (isset($data->id)){{ method_field('PATCH') }}@endif
                        <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="formGroupExampleInput" class="form-label">Add patient</label>
                                <select name="patient_id" id="cars" class="form-select form-control" required>
                                    <option value="" style="display: none">Choose patient</option>
                                    @foreach ($patients as $patient)
                                        <option value="{{ $patient['id'] }}" @if (isset($data->id)) {{ $patient['id'] == $data['patient_id'] ? 'selected' : '' }} @endif {{old('patient_id')==$patient['id']?'selected' : ''}}>
                                            {{ $patient['firstname'] }}</option>
                                    @endforeach
                                    <span style="color: red">
                                        @error('catagary'){{ $message }}<br>
                                        @enderror
                                    </span>
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="">select Doctor services</label>
                                <select class="form-select form-control" name="doctor_service_id" id="cars" required>
                                    <option value="" style="display: none">Doctor services</option>
                                    @foreach ($doctor_Services as $doctor_service)
                                        <option value="{{ $doctor_service['id'] }}" @if (isset($data->id)) {{ $doctor_service['id'] == $data->doctor_service_id ? 'selected' : '' }} @endif {{old('doctor_service_id')==$doctor_service['id']?'selected' : ''}}>
                                            @foreach ($doctors as $doctor)
                                                @if ($doctor->id == $doctor_service['doctors_id'])
                                                    {{ $doctor['name'] }}
                                                @endif
                                            @endforeach
                                            @foreach ($services as $service)
                                                @if ($service->id == $doctor_service['services_id'])
                                                    {{ '(' . $service['s_name'] . ')' }}
                                                @endif
                                            @endforeach
                                        </option>
                                    @endforeach
                                    <span style="color: red">
                                        @error('docto_id'){{ $message }}<br>
                                        @enderror
                                    </span>
                                </select>
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label for="">Appoiment date</label>
                                <input type="date" name="date" class="form-control" 
                                    value=@if (isset($data->id)) {{$data['date']}} @endif >
                            </div>
                        </div>
                        <div class="d-block text-right card-footer ">
                            <button type="submit" class="btn btn-primary active add-update">
                                @if (isset($data->id))
                                    {{ 'update' }}@else{{ 'add' }}
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
