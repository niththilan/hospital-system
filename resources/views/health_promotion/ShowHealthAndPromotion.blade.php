@extends('master.layout')

@section('content')
<style>


</style>
<div class="app-page-title">
    <div class="page-title-wrapper  ml-3 mr-3">
        <div class="page-title-heading">
            <div>Health And Promotion
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block ">

                <button type="button" class="btn-shadow      btn btn-info" onclick="window.location.href='/addHealthAndPromotion'">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="icon ion-android-add-circle"></i>
                    </span>
                    Add
                </button>

            </div>
        </div>
    </div>
    <div class="col-md-12 mt-3">
        <div class="main-card mb-3 card">
            <div class="card-body table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 20px">No</th>
                            <th scope="col" style="width: 150px">Image</th>
                            <th scope="col">name</th>
                            <th scope="col">Short description</th>
                            <th scope="col">Long description</th>
                            <th scope="col" style="width: 90px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><img src="/image/healthAndPromotion/{{ $item->image }}" alt="" style="height: 80px"></td>
                                <td>{{ $item['details'] }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['s_description'] }}</td>
                                <td>{{ $item['l_description'] }}</td>
                                <td><div style="display: flex; ">
                                    <i onclick="window.location.href='/healthAndPromotion/{{ $item['id'] }}'" class="bi bi-pencil-square icon1"  style="color:rgb(0, 8, 240); font-size:20px"></i>
                                    <i onclick="window.location.href='/deleteHealthAndPromotion/{{ $item['id'] }}'" class="bi bi-trash-fill icon1 ml-3" style="color:red; font-size:20px"></i>

                                    </div>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
