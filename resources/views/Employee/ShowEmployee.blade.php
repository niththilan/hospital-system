@extends('master.layout')
@section('content')
    <style>
        .table {
            /* width: 1000px; */
            table-layout: inherit;
            font-size: 3mm
        }

        .err {
            padding: 2px;
            /* background-color: rgb(230, 134, 134); */
            text-align: center;
        }

    </style>
    <div class="app-page-title ">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Employee
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">

                    <button type="button" class="btn-shadow  btn btn-info" onclick="window.location.href='/addEmployee'"
                        id="add">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                </div>
            </div>
        </div>
        @if (\Session::has('exp'))
            <div class="alert alert-danger fade-message err mt-3 ml-3 mr-3 ">
                <p>{{ \Session::get('exp') }}</p>
            </div><br />
            <script>
                $(function() {
                    setTimeout(function() {
                        $('.fade-message').slideUp();
                    }, 5000);
                });
            </script>
        @endif
        <div class="col-md-12 ">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table " style="">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 20px">No</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Image</th>
                                <th scope="col">Mobile Number</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Designation</th>
                                <th scope="col">City</th>
                                <th scope="col">catacory</th>
                                <th scope="col" style="width: 90px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $item['name'] }}</td>
                                    <td>{{ $item['email'] }}</td>
                                    <td><img src="/image/employee/{{ $item->image }}" alt="" style="height: 40px"></td>
                                    <td>{{ $item['mobile_number'] }}</td>
                                    <td>{{ $item['gender'] }}</td>
                                    @foreach ($role as $userRole)
                                        @if ($item['designation_id'] === $userRole['id'])
                                            <td>{{ $userRole['designation'] }}</td>
                                        @endif
                                    @endforeach
                                    <td>{{ $item['city'] }}</td>
                                    @foreach ($catacarys as $catacary)
                                        @if ($catacary['id']==$item['catagary_id'])
                                        <td>{{ $catacary['catacary'] }}</td>
                                        @endif
                                    @endforeach
                                    <td>
                                        <div style="display: flex; ">
                                            @if ($item['email'] != $constEmail || session('email') == $constEmail)
                                                <i onclick="window.location.href='/employee/{{ $item['id'] }}'"
                                                    class="bi bi-pencil-square"
                                                    style="color:rgb(0, 8, 240); font-size:20px"></i>
                                            @endif
                                            @if ($item['email'] != $constEmail)
                                                <i class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"
                                                    onclick="window.location.href='/deleteEmployee/{{ $item['id'] }}'"></i>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
