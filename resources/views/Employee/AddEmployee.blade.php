@extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id))
                        {{ 'Update Employee' }}@else{{ 'Add Employee' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/employee'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back   e"></i>
                        </span>
                        back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/employee" method="POST" class="container" enctype="multipart/form-data">
                        @csrf
                        @if (isset($data->id)){{ method_field('PATCH') }}@endif
                        <input type="hidden" name="id" value="@if (isset($data->id)){{ $data->id }}@endif">
                        {{-- <div class="mb-3"> --}}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="formGroupExampleInput2" class="form-label">Name</label>
                                <input type="" name="name" class="form-control" id="formGroupExampleInput2"
                                    placeholder="First Name " value="@if (isset($data->name)){{ $data->name }}@else{{ old('name') }}@endif">
                                <span style="color: red">
                                    @error('name'){{ $message }}<br>
                                    @enderror
                                </span>
                            </div>
                            <div class="form-group col-md-6">
                                @if (!isset($data->email))
                                    <label for="formGroupExampleInput2" class="form-label">Email</label>
                                    <input type="text" name="email" class="form-control" id="formGroupExampleInput2"
                                        placeholder="email " value="{{ old('email') }}">
                                    <span style="color: red">
                                        @error('email'){{ $message }}<br>
                                        @enderror
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="form-group col-md-6">
                                <label for="formGroupExampleInput" class="form-label">Mobile Number</label>
                                <input type="text" name="mobile_number" class="form-control" id="formGroupExampleInput2"
                                    placeholder="Add mobile_number" value="@if (isset($data->mobile_number)){{ $data->mobile_number }}@else{{ old('mobile_number') }}@endif">
                                <span style="color: red">
                                    @error('mobile_number'){{ $message }}<br>
                                    @enderror
                                </span>
                            </div>
                            <div class="form-group col-md-6"> <label for="formGroupExampleInput"
                                    class="form-label">City</label>
                                <input type="text" name="city" class="form-control" id="formGroupExampleInput"
                                    placeholder="City" value="@if (isset($data->city)){{ $data->city }}@else{{ old('city') }}@endif">
                                <span style="color: red">
                                    @error('city'){{ $message }}<br>
                                    @enderror
                                </span>

                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="form-group col-md-3">
                                <label for="formGroupExampleInput" class="form-label">photo</label>
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" name="imageUpload"
                                        class="form-control imageUpload" />
                                    <input type="hidden" name="base64image" name="base64image" id="base64image">
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview container2 mt-3">

                                    <div id="imagePreview">
                                    </div>
                                </div>
                                <span style="color: red">
                                    @error('image'){{ $message }}<br>
                                    @enderror
                                </span>

                            </div>
                            <div class="form-group col-md-3">
                                <label for="formGroupExampleInput" class="form-label">Gender </label>
                                <select name="gender" id="cars" class="form-control form-select">
                                    <option style="display: none">Choose Gender</option>
                                    @foreach ($gender as $gen)
                                        {{-- selected="@if ($data['designation_id'] === $item['id']){{'selected'}} @endif" --}}
                                        <option value="{{ $gen }}" @if (isset($data->id)) {{ $gen == $data['gender'] ? 'selected' : '' }} @endif>{{ $gen }}
                                        </option>
                                    @endforeach
                                    <span style="color: red">
                                        @error('gender'){{ $message }}<br>
                                        @enderror
                                    </span>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="formGroupExampleInput" class="form-label">Designation </label>
                                <select name="designation_id" id="cars" class="form-control form-select">
                                    <option value="" style="display: none">Choose Designation</option>
                                    @foreach ($role as $item)
                                        <option value="{{ $item['id'] }}" @if (isset($data->id)) {{ $item['id'] == $data['designation_id'] ? 'selected' : '' }} @endif>
                                            {{ $item['designation'] }}</option>
                                    @endforeach
                                    <span style="color: red">
                                        @error('designation_id'){{ $message }}<br>
                                        @enderror
                                    </span>
                                </select>
                            </div>
                            {{-- @foreach ($catagarys as $catagary)
                                {{$catagary}}
                                @endforeach --}}


                            <div class="form-group col-md-3">
                                <label for="formGroupExampleInput" class="form-label">Catagary</label>
                                <select name="catagary_id" id="cars" class="form-select form-control">
                                    <option style="display: none">Choose Catagary</option>
                                    @foreach ($catagarys as $catagary)
                                        {{-- selected="@if ($data['designation_id'] === $item['id']){{'selected'}} @endif" --}}
                                        <option name="catacary" value="{{ $catagary['id'] }}" @if (isset($data->id)) {{ $catagary['id'] == $data['catagary_id'] ? 'selected' : '' }} @endif>
                                            {{ $catagary['catacary'] }}</option>
                                    @endforeach
                                    <span style="color: red">
                                        @error('catagary'){{ $message }}<br>
                                        @enderror
                                    </span>
                                </select>
                            </div>
                        </div>

                        <div class="form-row mt-2">
                            @if (!isset($data->id) && session('email') == $constEmail)
                                <div class="form-group col-md-6">
                                    <label for="password" :value="__('Password')">Password</label>

                                    <input id="password" class="form-control" type="password" name="password"
                                        placeholder="enter password" @if (!isset($data->id))  @endif autocomplete="new-password">
                                    <span style="color: red">
                                        @error('password'){{ $message }}<br>
                                        @enderror
                                    </span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="password_confirmation" :value="__('Confirm Password')">Confirm
                                        Password</label>
                                    <input id="password_confirmation" class="form-control" type="password"
                                        name="password_confirmation" @if (!isset($data->id)) placeholder="confirm password"
                            @endif />
                        </div>
                        @endif

                </div>

                <div class="d-block text-right card-footer ">
                    <button type="submit" class="btn btn-primary active mt-3">
                        @if (isset($data->id)){{ 'update' }}@else{{ 'add' }}
                        @endif
                    </button>
                </div>
                </form>
            </div>
        </div>

    </div>
@endsection

<div class="modal fade bd-example-modal-lg imagecrop" id="model" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-11">
                            <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary crop" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script>
    var $modal = $('.imagecrop');
    var image = document.getElementById('image');
    var cropper;
    $("body").on("change", ".imageUpload", function(e) {
        var files = e.target.files;
        // console.log('name'+files);
        // console.log(URL);
        var done = function(url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                // console.log(URL);
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                console.log(URL);
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });
    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            viewMode: 1,
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });
    $("body").on("click", "#crop", function() {
        canvas = cropper.getCroppedCanvas({
            // width: 140,
            // height: 140,
        });
        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                $('#base64image').val(base64data);
                document.getElementById('imagePreview').style.backgroundImage = "url(" +
                    base64data + ")";
                $modal.modal('hide');
            }
        });
    })
</script>
