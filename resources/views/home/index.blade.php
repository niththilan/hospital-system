@extends('master.layout')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Home/slider image
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">

                        <button type="button" class="btn-shadow      btn btn-info"
                            onclick="window.location.href='{{ route('home.create') }}'">
                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                <i class="icon ion-android-add-circle"></i>
                            </span>
                            Add
                        </button>

                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Image</th>
                            <th scope="col" style="width: 12%">Action</th>

                        </tr>
                        @foreach ($homes as $home)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td><img src="/image/{{ $home->image }}" width="100px"></td>
                                <td>
                                    <div style="display: flex; flex-direction: row">
                                        <i onclick="window.location.href='{{ route('home.show', $home->id) }}'"
                                            class="bi bi-eye" style="color:rgb(0, 8, 240); font-size:20px"></i>
                                        {{-- <i onclick="window.location.href='{{ route('home.destroy',$home->id) }}'" class="bi bi-pencil-square ml-2" style="color:red; font-size:20px"></i> --}}
                                        <form action="{{ route('home.destroy', $home->id)}}" method="POST" class="ml-2">
                                            @csrf
                                            @method('DELETE')
                                            {{-- <a class="btn btn-primary" href="{{ route('home.edit',$home->id) }}">Edit</a> --}}

                                            {{-- <i onclick="window.location.href='submit'"class="bi bi-display"  style="color:rgb(0, 8, 240); font-size:20px"></i> --}}
                                            <button type="submit" class="" style="background: transparent;border: 0px"><i  class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

<div class="modal fade" id="close2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger" id="exampleModalLabel">Are you sure want close session</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                session will close you can't make any changes
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger"
                id="yes_cancal" >yes</button>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

    $(document).ready(function(){
        console.log(1234);

        $(".delete1").click(function(){
                var id = $(this).attr('item-id');
                console.log('delete');
                console.log(id);
                $("#delete2").modal('show');

                $("#yes_delete").click(function(){
                    window.location.href='/deleteProjectAndDonation/'+id
                });
        });


    });
</script>
