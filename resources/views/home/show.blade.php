@extends('master.layout')

@section('content')


<div class="app-page-title">
    <div class="page-title-wrapper  ml-3 mr-3">
        <div class="page-title-heading">
            <div><h2> Show image</h2>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block ">

                <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='{{ route('home.index') }}'"
                    >
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="icon ion-android-arrow-back"></i>
                </span>
                Back
                </button>
            </div>
        </div>
    </div>

    <div class="col-md-12 mt-3">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <img src="/image/{{ $home->image }}" width="500px">

            </div>

        </div>
    </div>
</div>


@endsection
