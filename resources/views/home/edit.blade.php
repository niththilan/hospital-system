@extends('master.layout')

@section('content')
    <style>
        label {
            background-color: rgb(0, 77, 106);
            color: white;
            padding: 0.5rem;
            font-family: sans-serif;
            border-radius: 0.3rem;
            cursor: pointer;
            margin-top: 1rem;
        }

        #file-chosen {
            margin-left: 0.3rem;
            font-family: sans-serif;
        }

    </style>
    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>
                    <h2>Change image</h2>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">

                <form action="{{ route('home.update', $home->id) }}" method="POST" enctype="multipart/form-data">
                    <div class="card-body">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">

                                    <!-- actual upload which is hidden -->
                                    <input type="file" id="imgInp" name="image" hidden />

                                    <!-- our custom upload button -->
                                    <label for="imgInp">Choose Image</label>

                                    <!-- name of file chosen -->
                                    <span id="file-chosen">No Image chosen</span>
                                    <br>
                                    <strong style="margin-left: 30px">OLD Image:</strong>
                                    <img src="/image/{{ $home->image }}" width="300px">
                                    <strong style="margin-left: 30px">NEW Image:</strong>
                                    <img src="/image/{{ $home->image }}" width="300px" id="blah" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-block text-right card-footer">
                        <button class="mr-2 btn btn-link btn-sm"
                            onclick="window.location.href='{{ route('home.index') }}'">Cancel</button>
                        <button class="btn btn-success btn-lg " type="submit">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>



    <script>
        imgInp.onchange = evt => {
            const [file] = imgInp.files
            if (file) {
                blah.src = URL.createObjectURL(file)
            }
        }

        const actualBtn = document.getElementById('imgInp');
        const fileChosen = document.getElementById('file-chosen');
        actualBtn.addEventListener('change', function() {
            fileChosen.textContent = this.files[0].name
        })
    </script>
@endsection
