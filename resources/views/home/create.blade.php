@extends('master.layout')

@section('content')
    <style>
        .avatar-upload .avatar-preview {
            width: 384px;
            height: 192px;
            position: relative;
            overflow: hidden;
        }

        .avatar-upload .avatar-preview>div {
            width: 100%;
            height: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }


        #image {
            display: block;
            /* This rule is very important, please don't ignore this */
            max-width: 100%;
        }

    </style>
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <div>
                    <h2>Add New Image</h2>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info"
                        onclick="window.location.href='{{ route('home.index') }}'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back"></i>
                        </span>
                        Back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form enctype="multipart/form-data" action="{{ route('home.store') }}" method="POST"
                        class="avatar-upload">
                        @csrf
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" accept=".jpg,.png, .jpeg" name="imageUpload"
                                class=" imageUpload" />
                            <input type="hidden" name="base64image" id="base64image">
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview container2 mt-3">
                            <div id="imagePreview">
                            </div>
                        </div>
                        <div class="d-block text-right card-footer">
                            <button class="btn btn-success btn-sm mb-6" type="submit">Save</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection


<div class="modal fade bd-example-modal-lg imagecrop" id="model" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-11">
                            <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary crop" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script>
    var $modal = $('.imagecrop');
    var image = document.getElementById('image');
    var cropper;
    $("body").on("change", ".imageUpload", function(e) {
        var files = e.target.files;
        var done = function(url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function(e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });
    $modal.on('shown.bs.modal', function() {
        cropper = new Cropper(image, {
            aspectRatio: 2,
            viewMode: 1,
        });
    }).on('hidden.bs.modal', function() {
        cropper.destroy();
        cropper = null;
    });
    $("body").on("click", "#crop", function() {
        canvas = cropper.getCroppedCanvas({
            // width: 140,
            // height: 140,
        });
        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                $('#base64image').val(base64data);
                document.getElementById('imagePreview').style.backgroundImage = "url(" +
                    base64data + ")";
                $modal.modal('hide');
            }
        });
    })
</script>

