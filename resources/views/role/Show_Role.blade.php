@extends('master.layout')
@section('content')
<style>
.err {
    padding: 2px;
    /* background-color: rgb(230, 134, 134); */
    text-align: center;
}

</style>
    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Job role
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                        <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/addRole'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                </div>
            </div>
        </div>
        @if (\Session::has('success'))
        <div class="alert alert-danger fade-message err mt-3 ml-3 mr-3 ">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        <script>
            $(function() {
                setTimeout(function() {
                    $('.fade-message').slideUp();
                }, 5000);
            });
        </script>
    @endif
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 20px">No</th>
                                <th scope="col">Role</th>
                                <th scope="col" style="width: 90px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $item['designation'] }}</td>
                                    <td>
                                        <div style="display: flex; ">
                                            <i onclick="window.location.href='/role/{{ $item['id'] }}'"
                                                class="bi bi-pencil-square"
                                                style="color:rgb(0, 8, 240); font-size:20px"></i>
                                            <i onclick="window.location.href='/deleteRole/{{ $item['id'] }}'"
                                                class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"></i>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Role</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @csrf
                @if (isset($data->id)){{ method_field('PATCH') }}@endif
                <input type="text" name="id" id="id" hidden>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="formGroupExampleInput" class="form-label">Add Role</label>
                        <input type="text" name="designation" class="form-control" id="formGroupExampleInput"
                            placeholder="Designation" value="@if (isset($data->designation)) {{ $data->designation }}@else{{ old('designation') }} @endif"
                        >
                        <span style="color: red">
                            @error('designation'){{ $message }}<br>
                            @enderror
                        </span>
                    </div>
                </div>
                <button type="button" class="btn btn-primary">
                    @if (isset($data->id))
                        {{ 'update' }}@else{{ 'add' }}
                    @endif
                </button>
            </div>
        </div>
    </div>
</div>


{{-- USING AJEX --}}
{{-- @extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Job role
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info create" data-bs-toggle="modal"
                        data-bs-target="#exampleModal">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 20px">No</th>
                                <th scope="col">Role</th>
                                <th scope="col" style="width: 90px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection --}}
{{--
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Role</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="text" name="id" id="id" hidden>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="formGroupExampleInput" class="form-label">Add Role</label>
                        <input type="text" class="form-control" id="designation_id" placeholder="Designation" required>
                        <span style="color: red">
                            @error('designation'){{ $message }}<br>
                            @enderror
                        </span>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal" id="add-update">
                        add
                    </button>
                </div>
            </div>
        </div>
    </div>
</div> --}}

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}

{{-- <script>
    $(document).ready(function() {
        getRole();
        // add
        $(document).on('click', '#add-update', function(e) {
            e.preventDefault();
            var data = {
                'id': $('#id').val(),
                'designation': $('#designation_id').val()
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/role",
                data: data,
                dataType: "json",
                success: function(response) {
                    if (response.status == 400) {
                        alert('data not saved');
                    } else {
                        getRole();
                    }
                }
            });
        });
        // get and update
        function getRole() {
            $.ajax({
                type: "GET",
                url: "/role",
                dataType: "json",
                contentTye: 'application/json',
                success: function(response) {
                    console.log(response);
                    var i = 0;
                    $('tbody').html('');
                    $.each(response, function(index, item) {
                        var x = '<tr>\
                                <td>' + ++i + '</td>\
                                <td>' + item.designation + '</td>\
                                <td><i value=' + item.id + ' data-designation=' + item.designation +
                            ' class="bi bi-pencil-square updateId"  data-bs-toggle="modal" data-bs-target="#exampleModal" style="color:rgb(0, 8, 240); font-size:20px"></i><i value=' +
                            item.id + '  class="bi bi-trash-fill ml-2 getId" style="color:red; font-size:20px"></i></td>\
                                </tr > ';
                        $('tbody').append(x);
                    });

                    $('.updateId').click(function() {
                        $('#id').val($(this).attr('value'));
                        $('#designation_id').val($(this).attr('data-designation'));
                        var id = $(this).attr('value');
                        $(document).on('click', '#add-update', function(e) {
                            var data = {
                                'id': $('#id').val(),
                                'designation': $('#designation_id').val()
                            }
                            console.log(data);
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $(
                                        'meta[name="csrf-token"]').attr(
                                        'content'),
                                    'X-HTTP-Method-Override': 'PATCH'
                                }
                            });
                            $.ajax({
                                type: "PATCH",
                                url: "/role/" + id,
                                data: data,
                                dataType: "json",
                                success: function(response) {
                                    getRole();
                                }
                            });

                        })
                    });
                }
            })
        }
        // delete
        $(document).on('click', '.getId', function(e) {
            e.preventDefault();
            var role_id = $(this).attr('value');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "get",
                url: "/deleteRole/" + role_id,
                success: function(response) {
                    getRole();
                }
            })
        })
        // clear the input fields
        $('.create').click(function() {
            $('#id').val('');
            $('#designation_id').val('');
        });
    });
</script> --}}
