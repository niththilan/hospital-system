@extends('master.layout')
@section('content')

    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id)){{ 'Update Role' }}@else{{ 'Add Role' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/role'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back"></i>
                        </span>
                        Back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/role" method="POST" class="container">
                    @csrf
                    @if (isset($data->id)){{ method_field('PATCH') }}@endif
                    <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="formGroupExampleInput" class="form-label">Add Role</label>
                            <input type="text" name="designation" class="form-control" id="formGroupExampleInput"
                                placeholder="Designation" value="@if (isset($data->designation)) {{ $data->designation }}@else{{ old('designation') }} @endif"
                            >
                            <span style="color: red">
                                @error('designation'){{ $message }}<br>
                                @enderror
                            </span>
                        </div>
                    </div>
                    <div class="d-block text-right card-footer ">
                        <button type="submit" class="btn btn-primary active add-update">
                            @if (isset($data->id))
                                {{ 'update' }}@else{{ 'add' }}
                            @endif
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
