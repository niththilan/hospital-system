<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('hospital-16.jpg') }}" type="image/gif" sizes="16x16">
    <title>Hospital System</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"
        integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
    <meta name="msapplication-tap-highlight" content="no">
    <link href="{{ asset('base.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('../assets/js/scrollbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('../assets/js/vendor/scrollbar.js') }}"></script>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
        <!--Header START-->
        <div class="app-header header-shadow bg-focus header-text-light">
            <div class="app-header__logo">
                {{-- <div class="logo-src"></div> --}}
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <div class="btn-group">
                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                        <img width="42" class="rounded-circle" src="/image/employee/{{ session('image') }}" alt="">
                    </a>
                </div>
                <div class="ml-2 " style="color: white">
                    <div class="">
                        {{ session('name') }}
                    </div>
                </div>
                <div class="header-btn-lg pr-0 " style="margin-right: -30px">
                    <form method="POST" action="{{ route('logout') }}" style=" margin-left: -30px">
                        @csrf
                        <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                            this.closest('form').submit();">
                            <div style="color: darkgray;" class="btn">
                                {{ __('Log Out') }}
                            </div>
                        </x-dropdown-link>
                    </form>
                </div>
            </div>
            <div class="app-header__content">
                <div class="app-header-left">
                </div>
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            class="p-0 btn">
                                            <img width="42" class="rounded-circle"
                                                src="/image/employee/{{ session('image') }}" alt="user image">
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info ">
                                    <div class="widget-heading">
                                        {{ session('name') }}
                                    </div>
                                </div>
                                <div class="widget-content-right header-user-info ml-3">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-btn-lg pr-0 ">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                <div style="color: darkgray;" class="btn">
                                    {{ __('Log Out') }}
                                </div>
                            </x-dropdown-link>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Header END-->
        {{-- sidebar Start --}}
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow bg-focus sidebar-text-light">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                                data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            {{-- <li class="app-sidebar__heading">Menu</li> --}}
                            <li>
                                <a href="/ddashboard">
                                    <i class="metismenu-icon bi bi-dash-square"></i>
                                    DASHBOARD
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-home"></i>
                                    Home
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul class="mm-show">
                                    <li>
                                        <a href="/home">
                                            <i class="metismenu-icon"></i>
                                            Slider image
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/chairmen">
                                            <i class="metismenu-icon">
                                            </i>Chairmen message
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-users"></i>
                                    Employee
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul class="mm-show">
                                    <li>
                                        <a href="/employee">
                                            <i class="metismenu-icon"></i>
                                            Employee
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/doctor">
                                            <i class="metismenu-icon">
                                            </i>doctor
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/doctorSpecialist">
                                            <i class="metismenu-icon"></i>
                                            Doctor Specialist
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/users">
                                            <i class="metismenu-icon"></i>
                                            user
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/patient">
                                            <i class="metismenu-icon"></i>
                                            Patient
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/getBooking">
                                            <i class="metismenu-icon"></i>
                                            Booking
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/role">
                                            <i class="metismenu-icon"></i>
                                            Job Roll
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-plugin"></i>
                                    Applications
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul class="mm-show">
                                    <li>
                                        <a href="/services">
                                            <i class="metismenu-icon">
                                            </i>services
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/doctorServices">
                                            <i class="metismenu-icon">
                                            </i>Doctor Services
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/timeTable">
                                            <i class="metismenu-icon">
                                            </i>timeTable
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/newsAndEvents">
                                            <i class="metismenu-icon">
                                            </i>news and event
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/healthAndPromotion">
                                            <i class="metismenu-icon">
                                            </i>helth and promotion
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/projectAndDonation">
                                            <i class="metismenu-icon">
                                            </i>project and donation
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!--DRAWER START-->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/metismenu"></script>
    <script src="../assets/js/scripts-init/app.js"></script>
    <script type="text/javascript" src="{{ asset('../assets/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('../assets/js/demo.js') }}"></script>


</body>

</html>
