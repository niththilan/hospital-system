@extends('master.layout')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.css" integrity="sha512-03p8fFZpOREY+YEQKSxxretkFih/D3AVX5Uw16CAaJRg14x9WOF18ZGYUnEqIpIqjxxgLlKgIB2kKIjiOD6++w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id))
                        {{ 'Update Doctor Specialist' }}@else{{ 'Add Doctor Specialist' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info"
                        onclick="window.location.href='/doctorSpecialist'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back"></i>
                        </span>
                        Back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/doctorSpecialist" method="POST" class="container">
                        @csrf
                        @if (isset($data->id)){{ method_field('PATCH') }}@endif
                        <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="formGroupExampleInput" class="form-label">Add Doctor</label>
                                <select name="doctor_id" id="cars" class="form-select form-control" required>
                                    <option value="" style="display: none">Choose Doctor</option>
                                    @foreach ($doctors as $doctor)
                                        <option value="{{ $doctor['id'] }}" @if (isset($data->id)) {{ $doctor['id'] == $data['doctor_id'] ? 'selected' : '' }} @endif>
                                            {{ $doctor['name'] }}</option>
                                    @endforeach
                                    <span style="color: red">
                                        @error('doctor_id'){{ 'doctor already have some specialist if you want to add more update there' }}<br>
                                        @enderror
                                    </span>
                                </select>
                            </div>
                            <span style="color: red">
                                @error('catagary'){{ $message }}<br>
                                @enderror
                            </span>
                            <div class="form-group col-md-3">
                                <label>Select Category :</label><br />
                                <select class="selectpicker form-control form-select" multiple name="secialist[]" required>
                                    @foreach ($specials as $special)
                                        <option value="{{ $special }}" @if (isset($data->id))
                                            @for ($i = 0; $i < sizeof($specials); $i++)
                                                {{ in_array($special, explode(',', $data->secialist)) ? 'selected' : '' }}
                                            @endfor
                                        @endif >
                                        {{ $special }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <span style="color: red">
                            @error('doctor_id'){{ 'doctor already have some specialist if you want to add more update there' }}<br>
                            @enderror
                        </span>
                        <div class="d-block text-right card-footer ">
                            <button type="submit" class="btn btn-primary active add-update">
                                @if (isset($data->id))
                                    {{ 'update' }}@else{{ 'add' }}
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

@endsection

