@extends('master.layout')
@section('content')
        <h1 style="color: green">
            @if (isset($data->id)){{ 'Update About' }}@else{{ 'Add About' }}
            @endif
        </h1>
    <form action="/about" method="POST" class="container">
        @csrf
        @if (isset($data->id)){{ method_field('PATCH') }}@endif
        <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
        <div class="mb-3">
            <label for="formGroupExampleInput" class="form-label">Employee</label>
            {{-- <input type="text" name="emp_id" class="form-control" id="formGroupExampleInput"
                placeholder="Add emp_id" value="@if (isset($data->emp_id)) {{ $data->emp_id }} @else{{ old('emp_id') }}@endif"
                >
            <span style="color: red">
                @error('emp_id'){{ $message }}<br>
                @enderror
            </span> --}}
            <select name="emp_id" id="cars">
                <option>Choose Employee</option>
                @foreach ($users as $user)
                    {{-- selected="@if ($data['designation_id'] === $item['id']){{'selected'}} @endif" --}}
                    <option value="{{ $user['id'] }}">{{ $user['name'] }}</option>

                @endforeach
                <span style="color: red">
                    @error('designation_id'){{ $message }}<br>
                    @enderror
                </span>
            </select>
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput2" class="form-label">Status </label>
            <input type="text" name="status" class="form-control" id="formGroupExampleInput2" placeholder="Add status"
                value="@if (isset($data->status)) {{ $data->status }}
            @else{{ old('status') }} @endif">
            <span style="color: red">
                @error('status'){{ $message }}<br>
                @enderror
            </span>
        </div>
        <button type="submit" class="btn btn-primary active">
            @if (isset($data->id)){{ 'update' }}@else{{ 'add' }}
            @endif
        </button>
    </form>
@endsection
