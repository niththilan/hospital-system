<!DOCTYPE html>
<html lang="zxx">
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Braintech - IT Solutions and Technology Startup HTML Template</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" type="image/x-icon" href="../assetsf/images/fav.png">
        <!-- Bootstrap v4.4.1 css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\bootstrap.min.css') }}">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\font-awesome.min.css') }}">
        <!-- flaticon css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\fonts\flaticon.css') }}" >
        <!-- animate css -->
        <link rel="stylesheet" type="text/css"  href= "{{ asset('..\assetsf\css\animate.css') }}" >
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\owl.carousel.css') }}" >
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\slick.css') }}" >
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\off-canvas.css') }}" >
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\magnific-popup.css') }}" >
        <!-- Main Menu css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\rsmenu-main.css') }}" >
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\rs-spacing.css') }}" >
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\style.css') }}" >
        <link rel="stylesheet" type="text/css" href="style.css"><!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href= "{{ asset('..\assetsf\css\responsive.css') }}" >
        <link rel="stylesheet" type="text/css" href="assetsf/css/..\assetsf\css\responsive.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    </head>
    <body class="defult-home">
      @include('frontEnd.master.header')
@yield('content')


@include('frontEnd.master.footer')
{{-- @include('frontEnd.master.js') --}}
        <!-- modernizr js -->
        <script src="{{ asset('/assetsf/js/modernizr-2.8.3.min.js') }}"></script>
        <!-- jquery latest version -->
        <script src="{{ asset('/assetsf/js/jquery.min.js') }}"></script>
        <!-- Bootstrap v4.4.1 js -->
        <script src="{{ asset('/assetsf/js/bootstrap.min.js') }}"></script>
        <!-- Menu js -->
        <script src="{{ asset('/assetsf/js/rsmenu-main.js') }}"></script>
        <!-- op nav js -->
        <script src="{{ asset('/assetsf/js/jquery.nav.js') }}"></script>
        <!-- owl.carousel js -->
        <script src="{{ asset('/assetsf/js/owl.carousel.min.js') }}"></script>
        <!-- wow js -->
        <script src="{{ asset('/assetsf/js/wow.min.js') }}"></script>
        <!-- Skill bar js -->
        <script src="{{ asset('/assetsf/js/skill.bars.jquery.js') }}"></script>
         <!-- counter top js -->
        <!-- swiper js -->
        <script src="{{ asset('/assetsf/js/swiper.min.js') }}"></script>
        <!-- particles js -->
        <script src="{{ asset('/assetsf/js/particles.min.js') }}"></script>
        <!-- magnific popup js -->
        <script src="{{ asset('/assetsf/js/jquery.magnific-popup.min.js') }}"></script>
        <!-- plugins js -->
        <script src="{{ asset('/assetsf/js/plugins.js') }}"></script>
        <!-- pointer js -->
        <script src="{{ asset('/assetsf/js/pointer.js') }}"></script>
        <!-- contact form js -->
        <script src="{{ asset('/assetsf/js/contact.form.js') }}"></script>
        <!-- main js -->
        <script src="{{ asset('/assetsf/js/main.js') }}"></script>
        {{-- <script>
            $('*').contents().each(function() {
          if(this.nodeType === Node.COMMENT_NODE) {
            $(this).remove();
          }
        });
        </script> --}}
        {{-- //jfoaj --}}

    </body>
</html>


