 <!-- Footer Start -->
 <footer id="rs-footer" class="rs-footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 footer-widget">
                    <div class="footer-logo mb-30">
                        <a href="index.html"><img src="assetsf/images/logo-dark.png" alt=""></a>
                    </div>
                      <div class="textwidget pb-30"><p>Sedut perspiciatis unde omnis iste natus error sitlutem acc usantium doloremque denounce with illo inventore veritatis</p>
                      </div>
                      <ul class="footer-social md-mb-30">
                          <li>
                              <a href="#" target="_blank"><span><i class="fa fa-facebook"></i></span></a>
                          </li>
                          <li>
                              <a href="# " target="_blank"><span><i class="fa fa-twitter"></i></span></a>
                          </li>

                          <li>
                              <a href="# " target="_blank"><span><i class="fa fa-pinterest-p"></i></span></a>
                          </li>
                          <li>
                              <a href="# " target="_blank"><span><i class="fa fa-instagram"></i></span></a>
                          </li>

                      </ul>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 pl-45 md-pl-15 md-mb-30">
                    <h3 class="widget-title">Our Services</h3>
                    <ul class="site-map">
                        {{-- <li><a href="software-development.html">Software Development</a></li>
                        <li><a href="web-development.html">Web Development</a></li>
                        <li><a href="web-development.html">Cloud and DevOps</a></li> --}}
                        @foreach ($Service as $item)

                        <li><a  href="/getServicesById/{{ $item['id'] }}">{{ $item->s_name }}</a></li>
                        @endforeach
                        <li><a href="/getServicesFive">view more services</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 md-mb-30">
                    <h3 class="widget-title">Contact Info</h3>
                    <ul class="address-widget">
                        <li>
                            <i class="flaticon-location"></i>
                            <div class="desc">374 FA Tower, William S Blvd 2721, IL, USA</div>
                        </li>
                        <li>
                            <i class="flaticon-call"></i>
                            <div class="desc">
                               <a href="tel:(+880)155-69569">(+880)155-69569</a>
                            </div>
                        </li>
                        <li>
                            <i class="flaticon-email"></i>
                            <div class="desc">
                                <a href="mailto:support@rstheme.com">support@rstheme.com</a>
                            </div>
                        </li>
                        <li>
                            <i class="flaticon-clock-1"></i>
                            <div class="desc">
                                Opening Hours: 10:00 - 18:00
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12">
                    <h3 class="widget-title">Newsletter</h3>
                    <p class="widget-desc">We denounce with righteous and in and dislike men who are so beguiled and demo realized.</p>
                    <p>
                        <input type="email" name="EMAIL" placeholder="Your email address" required="">
                        <em class="paper-plane"><input type="submit" value="Sign up"></em>
                        <i class="flaticon-send"></i>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row y-middle">
                <div class="col-lg-6 text-right md-mb-10 order-last">
                    <ul class="copy-right-menu">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="blog.html">Blog</a></li>
                        <li><a href="shop.html">Shop</a></li>
                        <li><a href="faq.html">FAQs</a></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>&copy; 2021 All Rights Reserved. Developed By <a href="http://rstheme.com/">RSTheme</a></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp" class="orange-color">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->

<!-- Search Modal Start -->
<div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
    <button type="button" class="close" data-bs-dismiss="modal">
        <span class="flaticon-cross"></span>
    </button>
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="search-block clearfix">
                <form>
                    <div class="form-group">
                        <input class="form-control" placeholder="Search Here..." type="text">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Search Modal End -->
