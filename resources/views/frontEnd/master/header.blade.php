
  <!--Preloader area start here-->
  <div id="loader" class="loader">
    <div class="loader-container"></div>
</div>
<!--Preloader area End here-->

<!-- Main content Start -->
<div class="main-content">
    <div class="full-width-header">
        <!--Header Start-->
        <header id="rs-header" class="rs-header">
            <!-- Topbar Area Start -->
            <div class="topbar-area">
                <div class="container">
                    <div class="row rs-vertical-middle">
                        <div class="col-lg-2">
                            <div class="logo-part">
                                <a href="index.html"><img src="../assetsf/images/logo-dark.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-10 text-right">
                            <ul class="rs-contact-info">
                                <li class="contact-part">
                                    <i class="flaticon-location"></i>
                                    <span class="contact-info">
                                        <span>Address</span>
                                        05 kandi BR. New York
                                    </span>
                                </li>
                                <li class="contact-part">
                                    <i class="flaticon-email"></i>
                                    <span class="contact-info">
                                        <span>E-mail</span>
                                        <a href="#"> support@rstheme.com</a>
                                    </span>
                                </li>
                                <li class="contact-part no-border">
                                     <i class="flaticon-call"></i>
                                    <span class="contact-info">
                                        <span>Phone</span>
                                         +019988772
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Topbar Area End -->

            <!-- Menu Start -->
            <div class="menu-area menu-sticky">
                <div class="container">
                    <div class="logo-area">
                        <a href="index.html">
                            <img class="sticky-logo" src="../assetsf/images/logo-dark.png" alt="logo">
                        </a>
                    </div>
                    <div class="rs-menu-area">
                        <div class="main-menu">
                            <div class="mobile-menu">
                                <a href="index.html" class="mobile-logo">
                                    <img src="../assetsf/images/logo-light.png" alt="logo">
                                </a>
                                <a href="#" class="rs-menu-toggle rs-menu-toggle-close">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </div>
                            <nav class="rs-menu">
                                <ul id="onepage-menu" class="nav-menu">
                                    <li>
                                        <a href="/fhome">Home</a>
                                    </li>

                                    <li>
                                        <a href="/getEmployeeByCatacary">About</a>
                                    </li>

                                    <li>
                                        <a href="/getServicesFive">Services</a>
                                    </li>


                                    <li>
                                        <a href="/getProjectAndDonationFive">projects</a>
                                    </li>
                                    <li>
                                        <a href="/getNewsAndEventsFive">news</a>
                                    </li>
                                    <li>
                                        <a href="/showSearch">Book</a>
                                    </li>
                                    <li>
                                        <a href="/contact">Contact us</a>
                                    </li>
                                </ul> <!-- //.nav-menu -->
                            </nav>
                        </div> <!-- //.main-menu -->
                    </div>
                    <div class="expand-btn-inner search-icon hidden-sticky hidden-md">
                        {{-- <ul>
                            <li class="sidebarmenu-search">
                                <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                    <i class="flaticon-search"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="toolbar-sl-share">
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
            <!-- Menu End -->
        </header>
        <!--Header End-->
    </div>
</div>
