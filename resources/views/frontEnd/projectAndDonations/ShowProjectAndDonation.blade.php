
@extends('frontEnd.master.layout')

@section('content')

<div class="rs-team-Single  pb-100 md-pt-80 md-pb-60 m">
    <div class="container">
        <div class="btm-info-team">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="images-part">
                        <img src="/image/projectAndDonation/{{ $data->image }}" alt="images" >
                    </div>
                </div>
                <div class="col-lg-7 sm-pt-30">
                    <div class="con-info">
                        <span class="designation-info"> </span>
                        <h2 class="title">{{ $data->description }}</h2>
                        <div class="short-desc">
                            {{ $data->s_detail }}
                        </div>
                        <div class="ps-informations">
                            <ul class="personal-info">
                               <h6>We got  {{$data->donation}} rupees for this project</h6>

                                <li>
                                    <span><i class="flaticon-email"> </i> </span>
                                    <a href="mailto:claire@rstheme.com">claire@rstheme.com</a>
                                </li>

                                <li>
                                    <span><i class="flaticon-call"></i></span> (123) - 222
                                    -1452
                                </li>
                            </ul>
                            <ul class="social-info">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 pr-55 md-pr-15">
                <div class="project-con">
                    <h3>Biography</h3>
                    <p>{{ $data->l_detail }}</p>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

