@extends('frontEnd.master.layout')

@section('content')
    <div class="rs-breadcrumbs img3">
        <div class="breadcrumbs-inner text-center">
            <h1 class="page-title">Services</h1>
            <ul>
                <li title="Braintech - IT Solutions and Technology Startup HTML Template">
                    <a class="active" href="index.html">Home</a>
                </li>
                <li>Services</li>
            </ul>
        </div>
    </div>
    <div class="rs-services style2 pt-120 pb-120 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                @foreach ($datas as $item)
                    <div class="col-lg-4 col-md-6 mb-40">
                        <div class="flip-box-inner">
                            <div class="flip-box-wrap">
                                <div class="front-part">
                                    <div class="front-content-part">
                                        <div class="front-icon-part">
                                            <div class="icon-part">
                                                <img src="../assetsf/images/services/main-home/icons/1.png" alt="">
                                            </div>
                                        </div>
                                        <div class="front-title-part">
                                            <h3 class="title"><a
                                                    href="software-development.html">{{ $item->description }}</a></h3>
                                        </div>
                                        <div class="front-desc-part">
                                            <p>{{ Str::limit($item->s_detail, 100, $end = '...show more') }}
                                            </p>
                                        </div>
                                        <div>we got donation {{ $item->donation }}/=</div>
                                    </div>
                                </div>
                                <div class="back-front">
                                    <div class="back-front-content">
                                        <div class="back-title-part">
                                            <h3 class="back-title"><a
                                                    href="">{{ $item->description }}</a></h3>
                                        </div>
                                        <div class="back-desc-part">
                                            <a class="view-more" href="/getProjectAndDonationById/{{ $item->id }}'">
                                                <p class="back-desc">
                                                    {{ Str::limit($item->l_detail, 100, $end = '...show more') }}</p>
                                            </a>
                                        </div>
                                        <div class="">
                                            <a class="readon view-more"
                                                href="/getProjectAndDonationById/{{ $item->id }}'">showmore..</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
