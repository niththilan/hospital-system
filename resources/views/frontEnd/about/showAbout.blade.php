@extends('frontEnd.master.layout')
@section('content')
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs img1">
        <div class="breadcrumbs-inner text-center">
            <h1 class="page-title">About</h1>
            <ul>
                <li title="Braintech - IT Solutions and Technology Startup HTML Template">
                    <a class="active" href="/fhome">Home</a>
                </li>
                <li>About</li>
            </ul>
        </div>
    </div>
    <!-- About Section Start -->
    {{-- <div class="rs-about gray-color pt-120 pb-120 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 md-mb-30">
                    <div class="rs-animation-shape">
                        <div class="images">
                            <img src="../assetsf/images/about/about-3.png" alt="">
                        </div>
                        <div class="middle-image2">
                            <img class="dance3" src="../assetsf/images/about/effect-1.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 pl-60 md-pl-15">
                    <div class="contact-wrap">
                        <div class="sec-title mb-30">
                            <div class="sub-text style-bg">About Us</div>
                            <h2 class="title pb-38">
                                We Are Increasing Business Success With Technology
                            </h2>
                            <div class="desc pb-35">
                                Over 25 years working in IT services developing software applications and mobile apps for
                                clients all over the world.
                            </div>
                            <p class="margin-0 pb-15">
                                We denounce with righteous indignation and dislike men who are so beguiled and demoralized
                                by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the
                                pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their
                                duty through weakness of will, which is the same as saying.
                            </p>
                        </div>
                        <div class="btn-part">
                            <a class="readon learn-more" href="contact.html">Learn-More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="shape-image">
                <img class="top dance" src="../assetsF/images/about/dotted-3.png" alt="">
            </div>
        </div>
    </div> --}}
    <!-- About Section End -->
    <!-- About Section End -->
    <div class="rs-team pt-120 pb-120 md-pt-80 md-pb-80 xs-pb-54 mt-10">
        <div class="sec-title2 text-center mb-30">
            <span class="sub-text style-bg white-color">Team</span>
            <h2 class="title white-color">
                Managers
            </h2>
        </div>
        <div class="container">
            <div class="container">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true"
                    data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false"
                    data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1"
                    data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="2"
                    data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="2"
                    data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="3"
                    data-md-device-nav="false" data-md-device-dots="true">
                    @foreach ($employees as $employee)
                        @foreach ($employee as $emp)
                            @if ($emp->catagary_id == 2)

                                <div class="team-item-wrap">
                                    <div class="team-wrap">
                                        <div class="image-inner">
                                            <a href="single-team.html"><img src="/image/employee/{{ $emp->image }}"
                                                    alt=""></a>
                                        </div>
                                    </div>
                                    <div class="team-content text-center">
                                        <h4 class="person-name"><a href="single-team.html"> {{ $emp['name'] }}</a></h4>
                                        <span class="designation"> {{ $emp['catagary_id'] }}
                                        </span>

                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <!-- Team Section End -->

    <!-- Process Section Start -->
    <div class="rs-process style2 pt-120 pb-120 md-pt-80 md-pb-73">
        <div class="container">
            <div class="sec-title2 text-center mb-45">
                <span class="sub-text style-bg">Process</span>
                <h2 class="title title2">
                    Board Of Director
                </h2>
            </div>
            <div class="row">

                @foreach ($employees as $employee)
                    @foreach ($employee as $emp)
                        @if ($emp['catagary_id'] == 1)

                            <div class="col-lg-3 col-sm-6 md-mb-50">
                                <div class="addon-process">
                                    <div class="process-wrap">
                                        <div class="process-img">
                                            <img src="/image/employee/{{ $emp->image }}" alt="">
                                        </div>
                                        <div class="process-text">
                                            <h3 class="title"> {{ $emp['name'] }}
                                            </h3>
                                            {{ $emp['catagary_id'] }}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach

            </div>
        </div>
    </div>
    <!-- Process Section End -->
    <h1 class="" style="text-align: center">Comminication staff</h1>
    <div class="rs-team modify1 pt-120 pb-95 md-pt-80 md-pb-75">
        <div class="container">
            <div class="row">

                @foreach ($employees as $employee)
                @foreach ($employee as $emp)

                    @if ($emp->catagary_id == 3)
                <div class="col-lg-4 col-md-6 mb-50">
                    <div class="team-item-wrap">
                        <div class="team-wrap" >
                            <div class="image-inner">
                                <a href="#"><img src="/image/employee/{{ $emp->image }}" alt="" style="height: 350px;  width: 400px; justify-content: center"></a>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4 class="person-name"><a href="single-team.html">  {{ $emp['name'] }}</a></h4>
                            <span class="designation">{{ $emp['catagary_id'] }}</span>
                            <ul class="team-social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>




    <!-- Main content End -->
@endsection
