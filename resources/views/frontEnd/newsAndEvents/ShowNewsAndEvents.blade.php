@extends('frontEnd.master.layout')

@section('content')

    <div class="rs-slider style3 gray-bg2 pt-120 pb-215 md-pt-80 md-pb-0">
        <div class="container">
            <div class="sec-title2 text-center mb-50">
                <h2 class="title testi-title">
                    The Pillars Of A Brand Strategy
                </h2>
                <p class="desc desc3">
                    Perspiciatis unde omnis iste natus error sit voluptatem accus antium doloremque laudantium, totam rem
                    aperiam,
                    eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </p>
            </div>
            @foreach ($datas as $data)

                <div class="rs-carousel owl-carousel mt-6 mb-6 " data-loop="true" data-items="1" data-margin="0"
                    data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800"
                    data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false"
                    data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false"
                    data-ipad-device="1" data-ipad-device-nav="true" data-ipad-device-dots="false" data-ipad-device2="1"
                    data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="1"
                    data-md-device-nav="true" data-md-device-dots="false"  style="margin-bottom: 50px">
                    <div class="row align-items-center">
                        <div class="col-lg-5 md-mb-50">
                            <div class="image-part">
                                <img src="/image/newsevetimage/{{ $data->image }}" alt="">
                            </div>
                        </div>
                        <div class="col-lg-7 pl-50 md-pl-15">
                            <div class="slider-content">
                                <div class="sec-title5 mb-20">
                                    <h2 class="title title2 pb-17"> {{ $data->name }}
                                    </h2>
                                    {{ $data->s_description }}
                                    <p class="desc2">
                                </div>
                                <ul class="check-square">
                                    <p>{{ Str::limit($data->l_description, 210, $end = '...') }}</p>
                                </ul>
                            </div>
                            <div>
                                <a href="/getNewsAndEventsById/{{ $data['id'] }}">show more</a>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>



@endsection
