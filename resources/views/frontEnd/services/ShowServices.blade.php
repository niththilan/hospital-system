@extends('frontEnd.master.layout')

@section('content')
<style>


.rs-services.style3 .services-item:hover {
  background-color: #FD660A;
}
.rs-services.style3 .services-item:hover.bga1 {
  background: #F30876;
}
.rs-services.style3 .services-item:hover.bga2 {
  background: #05DBEE;
}
.rs-services.style3 .services-item:hover.bga3 {
  background: #13E1AF;
}
.rs-services.style3 .services-item:hover.bga4 {
  background: #780FDA;
}
.rs-services.style3 .services-item:hover.bga5 {
  background: #0A99FD;
}
.rs-services.style3 .services-item:hover.bga6 {
  background: #2c44e4;
}
.rs-services.style3 .services-item:hover.bga7 {
  background: #f5be18;
}
.rs-services.style3 .services-item:hover.bga8 {
  background: #14d2f5;
}
.rs-services.style3 .services-item:hover.bga9 {
  background: #FD660A;
}
</style>
    <div class="rs-breadcrumbs img3">
        <div class="breadcrumbs-inner text-center">
            <h1 class="page-title">Services 2</h1>
            <ul>
                <li title="Braintech - IT Solutions and Technology Startup HTML Template">
                    <a class="active" href="">Home</a>
                </li>
                <li>Services 2</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumbs End -->

    <!-- Services Section Start -->
    <div class="rs-services style3 gray-color pt-120 pb-120 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                @foreach ($datas as $item)
                    <div class="col-lg-4 col-md-6 mb-20" >
                        <a href="/getServicesById/{{ $item['id'] }}">
                            <div class="services-item bga{{ $i++ }}" style="height: 400px">
                                <div class="services-icon">
                                    <div class="image-part">
                                        <img class="main-img" src="/image/services/{{ $item->image }}" alt=""
                                            style="height: 150px;width: auto;">
                                        <img class="hover-img" src="/image/services/{{ $item->image }}" alt=""
                                            style="height: 150px;width: auto;">
                                    </div>
                                </div>
                                <div class="services-content">
                                    <div class="services-text">
                                        <h3 class="title"><a href="software-development.html">{{ $item['s_name'] }}</a>
                                        </h3>
                                    </div>
                                    <div class="services-desc">
                                        <p>{{ Str::limit($item->s_description, 100, $end = '...') }}
                                        </p>
                                        <div class="sowmore" style="margin-bottom: -20px;margin-right: -200px"><a
                                                href="/getServicesById/{{ $item['id'] }}">show more....</a></div>
                                    </div>
                                    <div class="serial-number">
                                        {{ $j++ }}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Services Section End -->
@endsection
