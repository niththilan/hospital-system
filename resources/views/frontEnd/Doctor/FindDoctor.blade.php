@extends('frontEnd.master.layout')
@section('content')



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <style>
        h1 {
            font-size: 30px;
            color: #fff;
            text-transform: uppercase;
            font-weight: 300;
            text-align: center;
            margin-bottom: 15px;
        }

        table {
            width: 100%;
            table-layout: fixed;
        }

        .tbl-header {
            background-color: rgba(255, 255, 255, 0.3);
        }

        .tbl-content {
            height: 300px;
            overflow-x: auto;
            margin-top: 0px;
            /* border: 1px solid rgba(255,255,255,0.3); */
        }

        th {
            padding: 20px 15px;
            text-align: left;
            font-weight: 500;
            font-size: 12px;
            color: #fff;
            text-transform: uppercase;
        }

        td {
            padding: 15px;
            text-align: left;
            vertical-align: middle;
            font-weight: 300;
            font-size: 12px;
            color: #fff;
            border-bottom: solid 1px rgba(255, 255, 255, 0.1);
        }

        @media only screen and (max-width: 400px) {
            .jkj {
                display: flex;
                flex-direction: column;
            }
        }

        /* demo styles */

        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,300,700);

        body {}

        section {
            /* margin: 50px; */
            background: -webkit-linear-gradient(90deg, #03228f 10%, #0e73e4 100%);
            background: linear-gradient(90deg, #03228f 10%, #0e73e4 100%);
            font-family: 'Roboto', sans-serif;
        }


        /* follow me template */
        .made-with-love {
            margin-top: 40px;
            padding: 10px;
            clear: left;
            text-align: center;
            font-size: 10px;
            font-family: arial;
            color: #fff;
        }

        .made-with-love i {
            font-style: normal;
            color: #F50057;
            font-size: 14px;
            position: relative;
            top: 2px;
        }

        .made-with-love a {
            color: #fff;
            text-decoration: none;
        }

        .made-with-love a:hover {
            text-decoration: underline;
        }

        label {
            color: white;
        }

        ::-webkit-scrollbar {
            width: 6px;
        }

        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        }

        ::-webkit-scrollbar-thumb {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        }

    </style>

    <div class="rs-project bg5 style1 pt-12 md-pt-80">
        <div class="container  table-responsive" style="">
            <div class="sec-title2 text-center ">
                <h2 class="title white-color">
                    booking
                </h2>
            </div>
            <div class="form-row mt-0 jkj" style="display: flex">
                <div class="col-md-4 p-2">
                    <label for="inputage">Search</label>
                    <input id="myInput" class="form-control" name='q' type="search" placeholder="Search..">
                </div>
                <div class="col-md-4 p-2">
                    <label for="inputage">Services</label>
                    <select class="form-select form-control selectpicker" id="service" name="service"
                        aria-label="Default select example">
                        <option value="0" style="display: none">Choose Services</option>
                        <option value="0" style="">All Services</option>
                        @foreach ($data_sec as $data)
                            <option value="{{ $data->services_id }}">{{ $data->s_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4 p-2">
                    <label for="inputage">Doctors</label>
                    <select class="form-select form-control selectpicker" id="doctor" name="doctor"
                        aria-label="Default select example">
                        <option value="" style="display: none">Choose Doctor</option>
                        <option value="0" style="">All Doctors</option>
                        @foreach ($data_doc as $data)
                            <option value="{{ $data->doctor_id }}">{{ $data->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!--for demo wrap-->
            <div class="table-responsive">
                <div class="tbl-header mt-10 mb-0">
                    <table class="table" cellpadding="0" cellspacing="0" border="0">
                        <thead>

                        </thead>
                    </table>
                </div>
                <div class="tbl-content mb-9">
                    <table class="table" cellpadding="0" cellspacing="0" border="0">
                        <tbody>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script>
    $(window).on("load resize ", function() {
        var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
        $('.tbl-header').css({
            'padding-right': scrollWidth
        });
    }).resize();
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="number" name="services_id" id='getDoctorServicesId' hidden>
                @csrf
                <div class="form-row" style="display: flex">
                    <div class="col-md-6 p-2 ">
                        <label for="inputfirstname">First Name</label>
                        <input type="text" name="firstname" class="form-control" id="inputfirstname"
                            placeholder="firstname" required>
                    </div>
                    <div class="col-md-6 p-2">
                        <label for="inputlastname">Last Name</label>
                        <input type="text" name="lastname" class="form-control" id="inputlastname"
                            placeholder="lastname  " >
                    </div>
                </div>
                <div class="form-row" style="display: flex">

                    <div class="col-md-6 p-2 ">
                        <label for="inputage">Age</label>
                        <input type="number" name="age" class="form-control" id="inputage" placeholder="Age" pattern=""
                            onKeyPress="if(this.value.length==2) return false;" required>
                    </div>
                    <div class="col-md-6 p-2 ">
                        <label for="inputage">Gender</label>
                        <select class="form-select form-control selectpicker" id="gender" name="gender"
                            aria-label="Default select example" required>
                            <option value="" style="display: none">Gender</option>
                            @foreach ($gender as $item)
                                <option value='{{ $item }}'>{{ $item }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row" style="display: flex">
                    <div class="p-2 col-md-6">
                        <label for="inputphonenumber">Phone Number</label>
                        <input type="Number" name="phonenumber" class="form-control" id="inputphonenumber"
                            placeholder="Phone Number" onKeyPress="if(10<=this.value.length=<10) return false;"
                            required>
                    </div>
                    <div class="p-2 col-md-6">
                        <label for="inputemail">E-mail:optional</label>
                        <input type="email" name="email" class="form-control" id="inputemail" placeholder="E-mail">
                    </div>
                </div>
                <div class="form-row" style="display: flex">
                    <div class=" col-md-6 p-2">
                        <input type="text" name="" readonly for="inputApoiment" class="form-control-plaintext"
                            value="fix your day">
                    </div>
                    {{-- <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com"> --}}
                    <div class=" col-md-6 p-2">
                        <input type="date" name="date" class="form-control" id="inputApoiment">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="clickSearch" data-bs-dismiss="modal">book</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $.ajax({
            url: "/findAll",
            dataType: "json",
            success: function(response) {
                console.log(response.data);
                getDet(response)
            }
        });

        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();

            if (value != null) {
                $.ajax({
                    url: "/search",
                    data: {
                        q: value
                    },
                    dataType: "json",
                    success: function(response) {
                        getDet(response)
                    }
                });
            }
        });

        $('#service').change(function() {
            var id = $(this).val();
            $.ajax({
                url: "/serviceDropDown/" + id,
                dataType: "json",
                success: function(response) {
                    getDet(response)
                }
            });
        });

        $('#doctor').change(function() {
            var id = $(this).val();
            console.log(id);
            $.ajax({
                url: "/doctorDropDown/" + id,
                dataType: "json",
                success: function(response) {
                    console.log(response.data);
                    getDet(response)
                }
            });
        });

        function getDet(response) {
            i = 0;
            $('thead').html('');
            $('tbody').html('');

            var y = '<tr>\
                <th scope="col">' + 'Doctor Name' + '</th>\
                <th scope="col">' + 'Doctor Specialist' + '</th>\
                <th scope="col">' + 'Service' + '</th>\
                <th scope="col">' + 'Se' + '</th>\
                <th scope="col">' + 'monday' + '</th>\
                <th scope="col">' + 'tuesday' + '</th>\
                <th scope="col">' + 'wednesday' + '</th>\
                <th scope="col">' + 'thursday' + '</th>\
                <th scope="col">' + 'friday' + '</th>\
                <th scope="col">' + 'saturday' + '</th>\
                <th scope="col">' + 'sunday' + '</th>\
                <th scope="col">' + 'Action' + '</th>\  </tr>';
            $('thead').append(y);

            $.each(response.data, function(index, item) {
                if (item.monday != null) {
                    var arr = item.monday.split("_to_");
                } else {
                    var arr = ["", "-"]
                }
                if (item.tuesday != null) {
                    var arr1 = item.tuesday.split("_to_");
                } else {
                    var arr1 = ["", ""]
                }
                if (item.wednesday != null) {
                    var arr2 = item.wednesday.split("_to_");
                } else {
                    var arr2 = ["", "-"]
                }
                if (item.thursday != null) {
                    var arr3 = item.thursday.split("_to_");
                } else {
                    var arr3 = ["", "-"]
                }
                if (item.friday != null) {
                    var arr4 = item.friday.split("_to_");
                } else {
                    var arr4 = ["", "-"]
                }
                if (item.saturday != null) {
                    var arr5 = item.saturday.split("_to_");
                } else {
                    var arr5 = ["", "-"]
                }
                if (item.sunday != null) {
                    var arr6 = item.sunday.split("_to_");
                } else {
                    var arr6 = ["", "-"]
                }
                var x = '<tr>\
                                                <td>' + item.name + '</td>\
                                                <td>' + item.secialist + '</td>\
                                                <td>' + item.s_name + '</td>\
                                                <td>' + 'in' + '<br>' + 'out' + '</td>\
                                                <td>' + arr[0] + '<br>' + arr[1] + '</td>\
                                                <td>' + arr1[0] + '<br>' + arr1[1] + '</td>\
                                                <td>' + arr2[0] + '<br>' + arr2[1] + '</td>\
                                                <td>' + arr3[0] + '<br>' + arr3[1] + '</td>\
                                                <td>' + arr4[0] + '<br>' + arr4[1] + '</td>\
                                                <td>' + arr5[0] + '<br>' + arr5[1] + '</td>\
                                                <td>' + arr6[0] + '<br>' + arr6[1] + '</td>\
                                            <td><button  id="serviceid" doctor_service_id=' + item.doctor_service_id + '  data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-success" >book</button></td>\
                                        </tr > ';
                $('tbody').append(x);

            });
            $(document).on('click', '#serviceid', function(e) {
                $('#getDoctorServicesId').val($(this).attr('doctor_service_id'));
            });

            $(document).on('click', '#clickSearch', function(e) {
                e.preventDefault();
                var data = {
                    'firstname': $('#inputfirstname').val(),
                    'lastname': $('#inputlastname').val(),
                    'age': $('#inputage').val(),
                    'gender': $('#gender').val(),
                    'email': $('#inputemail').val(),
                    'phonenumber': $('#inputphonenumber').val(),
                    'date': $('#inputApoiment').val(),
                    'doctor_service_id': $('#getDoctorServicesId').val(),
                }
                console.log(data);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $(
                            'meta[name="csrf-token"]').attr(
                            'content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/bookingPatient",
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        // console.log(response);
                        if (response.status == 200) {
                            alert(
                                'Appoiment succesfully booked'
                            );
                        } else {
                            alert(
                                'Appoiment not booked, please fill correctly'
                            );
                        }
                    },
                });
            });

        }
    });
</script>
