@extends('frontEnd.master.layout')


@section('content')
    <div class="rs-slider style1">
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="0" data-autoplay="true"
            data-hoverpause="true" data-autoplay-timeout="3000" data-smart-speed="800" data-dots="false" data-nav="false"
            data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false"
            data-mobile-device-dots="false" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="false"
            data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="1"
            data-md-device-nav="true" data-md-device-dots="false">


            @foreach ($sliders as $item)

                <div class="slider-content slide{{ $j++ }}" style=" background: url('/image/{{ $item->image }}'); background-size: cover;
                        background-position: center;
                        background-repeat: no-repeat;
                        width: 100%;height: auto; z-index: -1;"></div>
            @endforeach
        </div>
        <!-- Slider Section End -->
        <!-- Services Section Start -->
        {{-- <div class="rs-services main-home style1 pt-100 md-pt-70">
            <div class="container">
                <div class="row">
                    @foreach ($Services as $item)

                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="services-item"> <a href="/getServicesById/{{ $item['id'] }}">
                                    <div class="services-icon">
                                        <div class="image-part">
                                            <img src="assetsf/images/services/style1/1.png" alt="">
                                        </div>
                                    </div>
                                    <div class="services-content">
                                        <div class="services-text">
                                            <h3 class="services-title">{{ $item['s_name'] }}</h3>
                                        </div>
                                        <div class="services-desc">
                                            <p>
                                                <a href="/getServicesFive">{{ Str::limit($item->s_description, 20, $end = '...') }}
                                                </a>
                                        </div>
                                        <div class="sowmore" style="margin-bottom: -20px;margin-right: -100px"><a
                                                href="/getServicesFive">show more....</a></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div> --}}
        <!-- Services Section End -->

<!-- Project Section Start -->
<div class="rs-project bg5 style1 pt-120 md-pt-80">
    <div class="container">
        <div class="sec-title2 text-center mb-45 md-mb-30">
            <span class="sub-text white-color">Project</span>
            <h2 class="title white-color">
                We Are Offering All Kinds of IT Solutions Services
            </h2>
        </div>
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true"
            data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false"
            data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1"
            data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2"
            data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2"
            data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3"
            data-md-device-nav="true" data-md-device-dots="false">

            @foreach ($Service as $item)

                <div class="project-item">
                    <div class="project-img">
                        <a href="/getServicesById/{{ $item['id'] }}"><img
                                src="/image/services/{{ $item->image }}" alt="images"
                                style="height: 400px; width: auto; margin: auto "></a>
                    </div>
                    <div class="project-content"><a href="/getServicesById/{{ $item['id'] }}">
                            <h3 class="title">{{ $item['s_name'] }}</h3>
                            <span class="category"><a
                                    href="/getServicesById/{{ $item['id'] }}">{{ Str::limit($item->s_description, 20, $end = '...') }}
                                </a></span>
                            <div class="sowmore" style="margin-bottom: -20px;margin-right: -200px"><a
                                    href="/getServicesById/{{ $item['id'] }}">show more....</a></div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Project Section End -->
        <!-- About Section Start -->
        @foreach ($messages as $item)

            <div class="rs-about bg4 pt-120 pb-120 md-pt-80 md-pb-80">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 md-mb-50">

                            <div class="images" style="height: 600px">
                                <img style="height: 400px; width: auto; margin-left: auto; margin-top: 100px; border-radius: 50% "
                                    src="/image/chairmen/{{ $item->image }}" alt="">
                            </div>

                        </div>
                        <div class="col-lg-6 pl-60 md-pl-15">
                            <div class="contact-wrap">
                                <div class="sec-title mb-30 ">
                                    <div class="sub-text style2">About Us</div>
                                    <h2 class="title pb-38">
                                        CHAIREMEN MESSAGE
                                    </h2>

                                    <p class="margin-0 pb-15">
                                        {{ $item->message }}

                                    </p>
                                </div>
                                {{-- <div class="btn-part">
                                    <a class="readon learn-more contact-us" href="contact.html">Learn More</a>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <!-- About Section End -->




    </div>
    <!-- Main content End -->
@endsection
