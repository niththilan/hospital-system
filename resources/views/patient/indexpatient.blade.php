@extends('master.layout')

@section('content')
    <style>
        body {
            font-family: Arial, Sans-serif;
        }

        .error {
            color: red;
            font-family: verdana, Helvetica;
        }

    </style>
    
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <div>patients
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">

                    <button type="button" class="btn-shadow      btn btn-info"
                        onclick="window.location.href='{{ route('patient.create') }}'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                </div>
            </div>
        </div>
        @if (\Session::has('exp'))
        <div class="alert alert-danger fade-message err mt-3 ml-3 mr-3 " >
            <p  style="text-align: center">{{ \Session::get('exp') }}</p>
        </div><br />
        <script>
            $(function() {
                setTimeout(function() {
                    $('.fade-message').slideUp();
                }, 5000);
            });
        </script>
        @endif  
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 15px">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Lasr Name</th>
                                <th scope="col" style="width: 15px">Age</th>
                                <th scope="col" style="width: 30px">Gender</th>
                                <th scope="col" style="width: 200px">E-mail</th>
                                <th scope="col" style="width: 110px">Phone number</th>
                                <th scope="col" style="width: 110px"> Action</th>
                            </tr>
                        </thead>
                        <tr>

                            @foreach ($Patients as $patient)
                        <tr>
                            <td>{{ $i-- }}</td>
                            <td>{{ $patient->firstname }}</td>
                            <td>{{ $patient->lastname }}</td>
                            <td>{{ $patient->age }}</td>
                            <td>{{ $patient->gender }}</td>
                            <td>{{ $patient->email }}</td>
                            <td>{{ $patient->phonenumber }}</td>
                            <td>
                                <div style="display: flex; ">
                                </div>
                                <form action="{{ route('patient.destroy', $patient->id) }}" method="POST" class="ml-2">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
