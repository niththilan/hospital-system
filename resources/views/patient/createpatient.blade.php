@extends('master.layout')
@section('content')
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
    </style>
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <div>Add New Patient
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info"
                        onclick="window.location.href='{{ route('patient.index') }}'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Cancel
                    </button>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="{{ route('patient.store') }}" method="POST" name="registration">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputfirstname">First Name</label>
                                <input type="text" name="firstname" class="form-control" id="inputfirstname"
                                    placeholder="firstname" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputlastname">Last Name (optional)</label>
                                <input type="text" name="lastname" class="form-control" id="inputlastname"
                                    placeholder="lastname  " >
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputemail">E-mail</label>
                                <input type="email" name="email" class="form-control" id="inputemail" placeholder="E-mail  "
                                    >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputphonenumber">Phone Number</label>
                                <input type="Number" name="phonenumber" class="form-control" id="inputphonenumber"
                                    placeholder="Phone Number" min="0"  onKeyPress="if(this.value.length==10) return false"
                                    required>
                            </div>

                        </div>
                        {{-- <div class="form-group col-md-6">
                            <label for="inputage">Doctor Name</label>
                            <select class="form-select form-control selectpicker" name="doctor_id"
                                aria-label="Default select example">
                                <option value="" style="display: none">Doctor Name</option>
                                @foreach ($doctors as $doctor)
                                    @foreach ($timeTableDoctor as $timeTable)
                                        @if ($timeTable->doctor_id == $doctor->id)
                                            <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            </select>
                        </div> --}}
                        <div class="form-row">
                            {{-- <div class="form-group col-md-6">
                                <label for="inputage">Services</label>
                                <select class="form-select form-control selectpicker" name="services_id"
                                    aria-label="Default select example">
                                    <option value="" style="display: none">Services</option>
                                    @foreach ($services as $service)
                                        @foreach ($timeTableService as $timeTable)
                                            @if ($timeTable->service_id == $service->id)
                                                <option value="{{ $service->id }}">{{ $service->s_name }}</option>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="form-group col-md-3">
                                <label for="inputage">Age</label>
                                <input type="number" name="age" class="form-control" id="inputage" placeholder="Age"
                                    pattern="" min="0" onKeyPress="if(this.value.length==2) return false;" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputage">Gender</label>
                                <select class="form-select form-control" name="gender"
                                    aria-label="Default select example" required>
                                    <option value="" style="display: none">Gender</option>
                                    @foreach ($gender as $item)
                                        <option value='{{ $item }}'>{{ $item }}</option>
                                    @endforeach

                                </select>
                            </div>


                        </div>

                        <div class="d-block text-right card-footer">


                            <button class="btn btn-info btn-lg btn-shadow" type="submit" active>save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
