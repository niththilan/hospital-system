{{-- @extends('master.layout')
@section('content')

    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Dash board
                </div>
            </div>

        </div>
        <div class="row mt-4">
            <div class="col-sm-12 col-md-6 col-xl-4">
                <div class="card mb-3 widget-chart">
                    <div class="widget-chart-content">
                        <div class="icon-wrapper rounded">
                            <div class="icon-wrapper-bg bg-warning"></div>
                            <i class="bi bi-people-fill"></i>
                        </div>
                        <div class="widget-numbers">
                            <span>{{ $totalappointment }}</span>
                        </div>
                        <div class="widget-subheading fsize-1 pt-2 opacity-10 text-warning font-weight-bold">PATIENTS</div>
                        <div class="widget-description opacity-8">
                        </div>
                    </div>
                    <div class="widget-chart-wrapper">
                        <div id="dashboard-sparklines-simple-1"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-xl-4">
                <div class="card mb-3 widget-chart">
                    <div class="widget-chart-content">
                        <div class="icon-wrapper rounded">
                            <div class="icon-wrapper-bg bg-danger"></div>
                            <i class="bi bi-file-medical-fill"></i>
                        </div>
                        <div class="widget-numbers"><span>{{ $doctors }}</span></div>
                        <div class="widget-subheading fsize-1 pt-2 opacity-10 text-danger font-weight-bold">
                            DOCTORS
                        </div>
                        <div class="widget-description opacity-8">
                        </div>
                    </div>
                    <div class="widget-chart-wrapper">
                        <div id="dashboard-sparklines-simple-2"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-xl-4">
                <div class="card mb-3 widget-chart">
                    <div class="widget-chart-content">
                        <div class="icon-wrapper rounded">
                            <div class="icon-wrapper-bg bg-info"></div>
                            <i class="bi bi-app-indicator"></i>
                        </div>
                        <div class="widget-numbers text-danger"><span>{{ $totalservices }}</span></div>
                        <div class="widget-subheading fsize-1 pt-2 opacity-10 text-info font-weight-bold">SERVICES</div>
                        <div class="widget-description opacity-8">
                        </div>
                    </div>
                    <div class="widget-chart-wrapper">
                        <div id="dashboard-sparklines-simple-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="app-page-title">
                <div class="col-md-12 mt-3">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">SERVICE</th>
                                        <th scope="col">APPOINTMENTS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($servicesGroup as $serviceGroup)
                                    @foreach ($services as $service)
                                        @if ($serviceGroup->service_id==$service->id)
                                        <tr>
                                            <td>{{ $service->s_name }}</td>
                                            <td>{{ $serviceGroup->service_count }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection --}}



@extends('master.layout')
@section('content')

    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Dash board
                </div>
            </div>

        </div>
        <div class="row mt-4">
            <div class="col-sm-12 col-md-6 col-xl-4">
                <div class="card mb-3 widget-chart">
                    <div class="widget-chart-content">
                        <div class="icon-wrapper rounded">
                            <div class="icon-wrapper-bg bg-warning"></div>
                            <i class="bi bi-people-fill"></i>
                        </div>
                        <div class="widget-numbers">
                            <span>{{ $totalappointment }}</span>
                        </div>
                        <div class="widget-subheading fsize-1 pt-2 opacity-10 text-warning font-weight-bold">PATIENTS</div>
                        <div class="widget-description opacity-8">
                        </div>
                    </div>
                    <div class="widget-chart-wrapper">
                        <div id="dashboard-sparklines-simple-1"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-xl-4">
                <div class="card mb-3 widget-chart">
                    <div class="widget-chart-content">
                        <div class="icon-wrapper rounded">
                            <div class="icon-wrapper-bg bg-danger"></div>
                            <i class="bi bi-file-medical-fill"></i>
                        </div>
                        <div class="widget-numbers"><span>{{ $doctors }}</span></div>
                        <div class="widget-subheading fsize-1 pt-2 opacity-10 text-danger font-weight-bold">
                            DOCTORS
                        </div>
                        <div class="widget-description opacity-8">
                        </div>
                    </div>
                    <div class="widget-chart-wrapper">
                        <div id="dashboard-sparklines-simple-2"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-xl-4">
                <div class="card mb-3 widget-chart">
                    <div class="widget-chart-content">
                        <div class="icon-wrapper rounded">
                            <div class="icon-wrapper-bg bg-info"></div>
                            <i class="bi bi-app-indicator"></i>
                        </div>
                        <div class="widget-numbers text-danger"><span>{{ $totalservices }}</span></div>
                        <div class="widget-subheading fsize-1 pt-2 opacity-10 text-info font-weight-bold">SERVICES</div>
                        <div class="widget-description opacity-8">
                        </div>
                    </div>
                    <div class="widget-chart-wrapper">
                        <div id="dashboard-sparklines-simple-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="app-page-title">
                <div class="col-md-12 mt-3">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">SERVICE</th>
                                        <th scope="col">APPOINTMENTS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($servicesGroup as $serviceGroup)
                                        <tr>
                                            {{-- <td>{{ $serviceGroup->s_name }}</td> --}}
                                            <td>{{ $serviceGroup->service_count }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
