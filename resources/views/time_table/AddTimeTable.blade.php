@extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id))
                        {{ 'Update Time Table' }}@else{{ 'Add Time Table' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/timeTable'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back   e"></i>
                        </span>
                        back
                    </button>
                </div>
            </div>
        </div>
       
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <form action="/timeTable" method="POST" class="container">
                        @csrf
                        @if (isset($data->id)){{ method_field('PATCH') }}@endif
                        <div class="form-row ">
                            <div class="form-group col-md-2">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="">select Doctor services</label>
                                <select class="form-select form-control" name="doctor_service_id" id="cars" required>
                                    <option value="" style="display: none">Doctor services</option>
                                    @foreach ($doctor_services as $doctor_service)
                                        <option value="{{ $doctor_service['id'] }}" @if (isset($data->id)) {{ $doctor_service['id'] == $data->doctor_service_id ? 'selected' : '' }} @endif {{old('doctor_service_id')==$doctor_service['id']?'selected' : ''}}>
                                            @foreach ($doctors as $doctor)
                                                @if ($doctor->id == $doctor_service['doctors_id'])
                                                    {{ $doctor['name'] }}
                                                @endif
                                            @endforeach

                                            @foreach ($services as $service)
                                                @if ($service->id == $doctor_service['services_id'])
                                                    {{ '(' . $service['s_name'] . ')' }}
                                                @endif
                                            @endforeach
                                        </option>
                                    @endforeach
                                    
                                </select>
                                <span style="color: red">
                                    @error('doctor_service_id'){{ 'doctor already have this service for week days,if time schedule change better to update  ' }}<br>
                                    @enderror
                                </span>
                            </div>
                           
                        </div>
                        <div class="form-row mt-2">
                            <div class="form-group col-md-2">
                            </div>
                            <div class="form-group col-md-2">
                                <label for="" class="" style="">Day </label>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="" class="" style="">Start Time</label>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="" class="" style="">End Time</label>
                            </div>
                        </div>
                        @foreach ($days as $day)
                            <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                            <div class="form-row   ">
                                <div class="form-group col-md-2">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="" class="" style="" name="day"
                                        value={{ $day }}>{{ $day }}</label>
                                </div>
                                @if (isset($data->id))
                                    <div class="form-group col-md-3">
                                        <input type="time" name="start_{{ $day }}" class="form-control"
                                            id="formGroupExampleInput" placeholder="Long description"
                                            value="{{ isset($data->$day) ? explode('_to_', $data->$day)[0] : '' }}">
                                        <span style="color: red">
                                            @error('start_time'){{ $message }}<br>
                                            @enderror
                                        </span>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="time" name="end_{{ $day }}" class="form-control"
                                            id="formGroupExampleInput"
                                            value="{{ isset($data->$day) ? explode('_to_', $data->$day)[1] : '' }}">
                                        <span style="color: red">
                                            @error('end_time'){{ $message }}<br>
                                            @enderror
                                        </span>
                                    </div>
                                @else
                                    <div class="form-group col-md-3">
                                        <input type="time" name="start_{{ $day }}" class="form-control"
                                            id="formGroupExampleInput" value="">
                                        <span style="color: red">
                                            @error('start_time'){{ $message }}<br>
                                            @enderror
                                        </span>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <input type="time" name="end_{{ $day }}" class="form-control"
                                            id="formGroupExampleInput" value="">
                                        <span style="color: red">
                                            @error('end_time'){{ $message }}<br>
                                            @enderror
                                        </span>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                        <div class="d-block text-right card-footer ">
                            <div class="col-md-11">
                                <button type="submit" class="btn btn-primary text-right  active">
                                    @if (isset($data->id))
                                        {{ 'update' }}@else{{ 'add' }}@endif
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
