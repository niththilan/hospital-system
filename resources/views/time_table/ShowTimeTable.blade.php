@extends('master.layout')
@section('content')

    <style>

    </style>
    <div class="app-page-title">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Add time table
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow      btn btn-info"
                        onclick="window.location.href='/addTimeTable'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                </div>
            </div>
        </div>

        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Doctor Service</th>
                                <th scope="col">monday</th>
                                <th scope="col">tuesday</th>
                                <th scope="col">wednesday</th>
                                <th scope="col">thursday</th>
                                <th scope="col">friday</th>
                                <th scope="col">saturday</th>
                                <th scope="col">sunday</th>
                                <th scope="col" style="width: 90px">Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>
                                        @foreach ($doctorServices as $doctorService)
                                            @if ($doctorService['id'] == $item['doctor_service_id'])
                                                @foreach ($doctors as $doctor)
                                                    @if ($doctor->id == $doctorService['doctors_id'])
                                                        {{ $doctor['name'] }}
                                                    @endif
                                                @endforeach

                                                @foreach ($services as $service)
                                                    @if ($service->id == $doctorService['services_id'])
                                                        {{ '(' . $service['s_name'] . ')' }}
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </td>

                                    @if ($item['monday'])
                                        <td>{{ explode('_to_', $item['monday'])[0] }} to
                                            {{ explode('_to_', $item['monday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    @if ($item['tuesday'])
                                        <td>{{ explode('_to_', $item['tuesday'])[0] }} to
                                            {{ explode('_to_', $item['tuesday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    @if ($item['wednesday'])
                                        <td>{{ explode('_to_', $item['wednesday'])[0] }} to
                                            {{ explode('_to_', $item['wednesday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    @if ($item['thursday'])
                                        <td>{{ explode('_to_', $item['thursday'])[0] }} to
                                            {{ explode('_to_', $item['thursday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    @if ($item['friday'])
                                        <td>{{ explode('_to_', $item['friday'])[0] }} to
                                            {{ explode('_to_', $item['friday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    @if ($item['saturday'])
                                        <td>{{ explode('_to_', $item['saturday'])[0] }} to
                                            {{ explode('_to_', $item['saturday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    @if ($item['sunday'])
                                        <td>{{ explode('_to_', $item['sunday'])[0] }} to
                                            {{ explode('_to_', $item['sunday'])[1] }}</td>
                                    @else
                                        <td> - </td>

                                    @endif
                                    <td>
                                        <div style="display: flex; ">
                                            <i onclick="window.location.href='/timeTable/{{ $item['id'] }}'"
                                                class="bi bi-pencil-square"
                                                style="color:rgb(0, 8, 240); font-size:20px"></i>
                                            <i onclick="window.location.href='/deleteTimeTable/{{ $item['id'] }}'"
                                                class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"></i>

                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

