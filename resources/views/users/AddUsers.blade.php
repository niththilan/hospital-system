@extends('master.layout')
@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper  ml-3 mr-3">
            <div class="page-title-heading">
                <h1 style="color: green">
                    @if (isset($data->id)){{ 'Update User' }}@else{{ 'Add User' }}
                    @endif
                </h1>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <button type="button" class="btn-shadow btn btn-info" onclick="window.location.href='/users'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-arrow-back   e"></i>
                        </span>
                        back
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body">

                    <form action="/users" method="POST" class="container" enctype="multipart/form-data">
                        @csrf
                        @if (isset($data->id)){{ method_field('PATCH') }}@endif
                        <input type="hidden" name="id" value="@if (isset($data->id)) {{ $data->id }} @endif">
                        @if (!isset($data->id))
                            <div class="mb-3">
                                <label for="formGroupExampleInput" class="form-label">User Name : </label>
                                <select name="email" id="cars" class="form-control form-select" style="width: 150px"
                                    required>
                                    <option value="" style="display: none">Choose Name</option>
                                    @foreach ($employees as $employee)
                                        @if ($employee->status == 0)
                                            <option value="{{ $employee['email'] }}" @if (isset($data->id)) {{ $employee['id'] == $data['emp_id'] ? 'selected' : '' }} @endif @if ($employee['email'] == old('email'))
                                                {{ $employee['id'] == $employee['id'] ? 'selected' : '' }}
                                        @endif
                                        >
                                        {{ $employee['name'] }}</option>
                                    @endif
                        @endforeach
                        <span style="color: red">
                            @error('nationality'){{ $message }}<br>
                            @enderror
                        </span>
                        </select>
                </div>
                @endif

                <div class="form-row mt-3">
                    <div class="form-group col-md-6">
                        <!-- New Password -->
                        <label for="password" :value="__('Password')">New Password</label>
                        <input id="password" class="form-control" type="password" name="password" required
                            autocomplete="new-password" placeholder="Enter Password">
                        <span style="color: red">
                            @error('password'){{ $message }}<br>
                            @enderror
                        </span>
                    </div>
                    <div class="form-group col-md-6">
                        <!-- Confirm Password -->
                        <label for="password_confirmation" :value="__('Confirm Password')">Confirm Password</label>
                        <input id="password_confirmation" class="form-control" type="password" name="password_confirmation"
                            required placeholder="Confirm Password" />
                    </div>
                </div>
                <div class="d-block text-right card-footer ">
                    <button type="submit" class="btn btn-primary active mt-3">
                        @if (isset($data->id)){{ 'update' }}@else{{ 'add' }}
                        @endif
                    </button>
                </div>

                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
