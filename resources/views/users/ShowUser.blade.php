@extends('master.layout')
@section('content')
    <style>
        .table {
            /* width: 1000px; */
            table-layout: inherit;
            font-size: 3mm
        }
    </style>
    <div class="app-page-title ">
        <div class="page-title-wrapper ml-3 mr-3">
            <div class="page-title-heading">
                <div>Users
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    @if (session('email')==$constEmail)
                    <button type="button" class="btn-shadow      btn btn-info" onclick="window.location.href='/addUser'">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="icon ion-android-add-circle"></i>
                        </span>
                        Add
                    </button>
                    @endif

                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <table class="table " style="">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 20px">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col" style="width: 90px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    {{-- <td>{{ $item['id'] }}</td> --}}
                                    <td>{{ ++$i }}</td>
                                    @foreach ($employees as $employee)
                                        @if ($item['emp_id']==$employee['id'])
                                    <td>{{ $employee['name']}}</td>

                                        @endif
                                    @endforeach
                                    <td>{{ $item['email'] }}</td>
                                    <td>
                                        <div style="display: flex; ">

                                            @if ($item['email'] == session('email') && session('email') != $constEmail)

                                                <i onclick="window.location.href='/users/{{ $item['id'] }}'"
                                                    class="bi bi-pencil-square"
                                                    style="color:rgb(0, 8, 240); font-size:20px"></i>


                                            @elseif (session('email')==$constEmail && $item['email'] !=$constEmail)
                                                {{-- <i onclick="window.location.href='/users/{{ $item['id'] }}'"
                                            class="bi bi-pencil-square"
                                            style="color:rgb(0, 8, 240); font-size:20px"></i> --}}
                                                <i onclick="window.location.href='/deleteUsers/{{ $item['id'] }}'"
                                                    class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"></i>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
