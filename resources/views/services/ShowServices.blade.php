@extends('master.layout')
@section('content')
<style>
    .err{
        padding: 2px;
        text-align: center;
    }
</style>
<div class="app-page-title">
    <div class="page-title-wrapper ml-3 mr-3">
        <div class="page-title-heading">
            <div>Add services
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block ">
                <button type="button" class="btn-shadow      btn btn-info" onclick="window.location.href='/addService'">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="icon ion-android-add-circle"></i>
                    </span>
                    Add
                </button>
            </div>
        </div>
    </div>
    @if (\Session::has('exp'))
    <div class="alert alert-danger fade-message err mt-3 ml-3 mr-3 " >
        <p>{{ \Session::get('exp') }}</p>
    </div><br />
    <script>
        $(function() {
            setTimeout(function() {
                $('.fade-message').slideUp();
            }, 5000);
        });
    </script>
    @endif
    <div class="col-md-12 mt-3">
        <div class="main-card mb-3 card">
            <div class="card-body table-responsive">
                <table class="table ">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">image</th>
                            <th scope="col">Short Description</th>
                            <th scope="col">Long Description</th>
                            <th scope="col" style="width: 90px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                {{-- <td>{{ $item['id'] }}</td> --}}
                                <td>{{++$i}}</td>
                                <td>{{ $item['s_name'] }}</td>
                                <td><img src="/image/services/{{ $item->image }}" alt="" style="height: 80px"></td>
                                <td>{{ $item['s_description'] }}</td>
                                <td>{{ $item['l_description'] }}</td>
                                <td>
                                    <div style="display: flex; ">
                                    <i onclick="window.location.href='/services/{{ $item['id'] }}'" class="bi bi-pencil-square"  style="color:rgb(0, 8, 240); font-size:20px"></i>
                                    <i onclick="window.location.href='/deleteServices/{{ $item['id'] }}'" class="bi bi-trash-fill ml-2" style="color:red; font-size:20px"></i>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
