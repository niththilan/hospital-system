<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthAndPromotion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_promotion', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('image')->nullable(false);
            $table->string('name')->nullable(false);
            $table->string('s_description')->nullable(false);
            $table->string('l_description')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_promotion');
    }
}
