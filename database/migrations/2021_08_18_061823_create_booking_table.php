<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('patient_id')->nullable(false)->unsigned();
            $table->bigInteger('doctor_service_id')->nullable(false)->unsigned();
            $table->date('date')->nullable(false);
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('doctor_service_id')->references('id')->on('doctorService');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
