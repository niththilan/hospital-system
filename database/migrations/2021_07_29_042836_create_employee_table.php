<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->id();
            $table->string('name',40)->nullable(false);
            $table->string('email')->unique();
            $table->string('image')->nullable(true);
            $table->integer('mobile_number',10)->nullable(false)->autoIncrement(false);
            $table->string('gender',6)->nullable(false);
            $table->bigInteger('designation_id')->nullable(false)->unsigned();
            $table->string('city',20)->nullable(false);
            $table->integer('catagary_id',1)->nullable(false)->autoIncrement(false);
            $table->foreign('designation_id')->references('id')->on('role');
            $table->integer('status')->nullable(false);
            $table->timestamps();
        });

        DB::table('employee')->insert(
            array(
                'name' => 'sn',
                'email' => 'sn@gmail.com',
                'image' => '20210823104347.jpg',
                'mobile_number' => 1234567890,
                'gender' => 'Male',
                'designation_id' => 1,
                'city' => 'Kili',
                'catagary_id' => 1,
                'status' => 1,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
