<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctorService', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('doctors_id')->nullable(false)->unsigned();
            $table->bigInteger('services_id')->nullable(false)->unsigned();
            $table->foreign('doctors_id')->references('id')->on('doctor');
            $table->foreign('services_id')->references('id')->on('services');
            $table->unique(['doctors_id', 'services_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctorService');
    }
}
