<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $days=['monday','tuesday','wednesday','thusday','friday','saturday','sunday'];

        Schema::create('time_table', function (Blueprint $table) {
            $table->id();
            $table->string('monday')->nullable(true);
            $table->string('tuesday')->nullable(true);
            $table->string('wednesday')->nullable(true);
            $table->string('thursday')->nullable(true);
            $table->string('friday')->nullable(true);
            $table->string('saturday')->nullable(true);
            $table->string('sunday')->nullable(true);   
            $table->bigInteger('doctor_service_id')->unsigned()->nullable(false);
            $table->foreign('doctor_service_id')->references('id')->on('doctorService');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('time_table');
    }
}
