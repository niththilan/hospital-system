<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectAndDonation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_donation', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('image')->nullable(false);
            $table->string('description')->nullable(false);
            $table->string('s_detail')->nullable(false);
            $table->text('l_detail')->nullable(false);
            $table->double('donation',10,2)->nullable(false);
            $table->string('status')->default('yes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_donation');
    }
}
