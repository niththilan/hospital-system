<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ChairmenController;
use App\Http\Controllers\contactController;
use App\Http\Controllers\ContactUsFormController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\NewsAndEvents;
use App\Http\Controllers\ProjectAndDonationController;
use App\Http\Controllers\HealthAndPromotionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TimeTableController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\LayoutController;
use App\Models\Patient;
use App\Models\Project_Donation;
use App\Http\Controllers\Booking;
use App\Http\Controllers\DoctorServices;
use App\Http\Controllers\DoctorSpecialist;
use App\Models\BookingModel;

Route::get('/fhome',[HomeController::class,'fhome']);
// Route::get('/tfhome',[HomeController::class,'footer']);

// User Front end

Route::get('/contact', [ContactUsFormController::class, 'createForm']);

Route::post('/contact', [ContactUsFormController::class, 'ContactUsForm'])->name('contact.store');
// employee
Route::get('/getEmployeeByCatacary',[EmployeeController::class,'findEmployeeByCatacary']);

// services
Route::get('/getServicesFive',[ServiceController::class,'getServicesFive']);
Route::get('/getServicesById/{id}',[ServiceController::class,'getServicesById']);
Route::get('/viewMoreServices',[ServiceController::class,'viewMore']);

//newsAndEvents
Route::get('/getNewsAndEventsFive',[NewsAndEvents::class,'getNewsAndEventsFive']);
Route::get('/getNewsAndEventsById/{id}',[NewsAndEvents::class,'getNewsAndEventsById']);
Route::get('/viewMoreNewsAndEvents',[NewsAndEvents::class,'viewMore']);

// project and donation
Route::get('/getProjectAndDonationFive',[ProjectAndDonationController::class,'getProjectAndDonationFive']);
Route::get('/getProjectAndDonationById/{id}',[ProjectAndDonationController::class,'getProjectAndDonationById']);
Route::get('/viewMoreProjectAndDonation',[ProjectAndDonationController::class,'viewMore']);

// doctor
Route::get('/search',[DoctorController::class,'search']);
Route::get('/showSearch',[DoctorController::class,'showSearch']);
Route::get('/serviceDropDown/{id}',[DoctorController::class,'serviceDropDown']);
Route::get('/doctorDropDown/{id}',[DoctorController::class,'doctorDropDown']);
Route::get('/findAll',[DoctorController::class,'findAll']);


// Patient
Route::post('/bookingPatient',[PatientController::class,'storeByPatient']);

require __DIR__.'/auth.php';

Route::group(['middleware' => 'auth'], function() {

Route::resource('home',HomeController::class);
Route::resource('patient',PatientController::class);
Route::resource('ddashboard',DashboardController::class);

Route::view('/addNewsAndEvents','news_events.AddNewsAndEvents');
Route::post('/newsAndEvents',[NewsAndEvents::class,'addNewsAndEvents']);
Route::get('/newsAndEvents',[NewsAndEvents::class,'getNewsAndEvents']);
Route::patch('/newsAndEvents',[NewsAndEvents::class,'updateNewsAndEvents']);
Route::get('/newsAndEvents/{id}',[NewsAndEvents::class,'editNewsAndEvents']);
Route::get('/deleteNewsAndEvents/{id}',[NewsAndEvents::class,'deleteNewsAndEvents']);

//project and donation
Route::view('/addProjectAndDonation','project_donation.AddProjectAndDonation');
Route::post('/projectAndDonation',[ProjectAndDonationController::class,'addProjectAndDonation']);
Route::get('/projectAndDonation',[ProjectAndDonationController::class,'getaddProjectAndDonation']);
Route::patch('/projectAndDonation',[ProjectAndDonationController::class,'updateProjectAndDonation']);
Route::get('/projectAndDonation/{id}',[ProjectAndDonationController::class,'editProjectAndDonation']);
Route::get('/deleteProjectAndDonation/{id}',[ProjectAndDonationController::class,'deleteProjectAndDonation']);
Route::patch('/addProjectAndDonation',[ProjectAndDonationController::class,'addDonation']);
Route::get('status/{id}',[ProjectAndDonationController::class,'changeStatus']);

//health and promotion
Route::view('/addHealthAndPromotion','health_promotion.AddHealthAndPromotion');
Route::post('/healthAndPromotion',[HealthAndPromotionController::class,'addHealthAndPromotion']);
Route::get('/healthAndPromotion',[HealthAndPromotionController::class,'getHealthAndPromotion']);
Route::patch('/healthAndPromotion',[HealthAndPromotionController::class,'updateHealthAndPromotion']);
Route::get('/healthAndPromotion/{id}',[HealthAndPromotionController::class,'editHealthAndPromotion']);
Route::get('/deleteHealthAndPromotion/{id}',[HealthAndPromotionController::class,'deleteHealthAndPromotion']);

//role
Route::view('/addRole','role.Add_Role');
Route::post('/role',[RoleController::class,'addRole']);
// Route::get('/getRole',[RoleController::class,'index']);
Route::get('/role',[RoleController::class,'getRole']);
Route::patch('/role',[RoleController::class,'updateRole']);
Route::get('/role/{id}',[RoleController::class,'editRole']);
Route::get('/deleteRole/{id}',[RoleController::class,'deleteRole']);

//service
Route::view('/addService','services.AddServices');
Route::post('/services',[ServiceController::class,'addServices']);
Route::get('/services',[ServiceController::class,'getServices']);
Route::patch('/services',[ServiceController::class,'updateServices']);
Route::get('/services/{id}',[ServiceController::class,'editServices']);
Route::get('/deleteServices/{id}',[ServiceController::class,'deleteServices']);

// user
// Route::view('/addUser','users.AddUsers');
Route::get('/addUser',[UsersController::class,'users']);
Route::post('/users',[UsersController::class,'addUsers']);
Route::get('/users',[UsersController::class,'getUsers']);
Route::patch('/users',[UsersController::class,'updateUsers']);
Route::get('/users/{id}',[UsersController::class,'editUsers']);
Route::get('/deleteUsers/{id}',[UsersController::class,'deleteUsers']);

//doctor
// Route::view('/addDoctor','doctor.AddDoctor');
Route::get('/addDoctor',[DoctorController::class,'doctor']);
Route::post('/doctor',[DoctorController::class,'addDoctor']);
Route::get('/doctor',[DoctorController::class,'getDoctor']);
Route::patch('/doctor',[DoctorController::class,'updateDoctor']);
Route::get('/doctor/{id}',[DoctorController::class,'editDoctor']);
Route::get('/deleteDoctor/{id}',[DoctorController::class,'deleteDoctor']);

//time table
// Route::view('/addTimeTable','time_table.AddTimeTable');
Route::get('/addTimeTable',[TimeTableController::class,'timeTable']);
Route::post('/timeTable',[TimeTableController::class,'addTimeTable']);
Route::get('/timeTable',[TimeTableController::class,'getTimeTable']);
Route::patch('/timeTable',[TimeTableController::class,'updateTimeTable']);
Route::get('/timeTable/{id}',[TimeTableController::class,'editTimeTable']);
Route::get('/deleteTimeTable/{id}',[TimeTableController::class,'deleteTimeTable']);

//chairman
// Route::view('/addChairmen','chairmen.AddChairmen');
Route::get('/addChairmen',[ChairmenController::class,'chairmen']);
Route::post('/chairmen',[ChairmenController::class,'addChairmen']);
Route::get('/chairmen',[ChairmenController::class,'getChairmen']);
Route::patch('/chairmen',[ChairmenController::class,'updateChairmen']);
Route::get('/chairmen/{id}',[ChairmenController::class,'editChairmen']);
Route::get('/deleteChairmen/{id}',[ChairmenController::class,'deleteChairmen']);

//about
// Route::view('/addAbout','about.AddAbout');
Route::get('/addAbout',[AboutController::class,'about']);
Route::post('/about',[AboutController::class,'addAbout']);
Route::get('/about',[AboutController::class,'getAbout']);
Route::patch('/about',[AboutController::class,'updateAbout']);
Route::get('/about/{id}',[AboutController::class,'editAbout']);
Route::get('/deleteAbout/{id}',[AboutController::class,'deleteAbout']);

// Employee
Route::get('/addEmployee',[EmployeeController::class,'employee']);
Route::post('/employee',[EmployeeController::class,'addEmployee']);
Route::get('/employee',[EmployeeController::class,'getEmployee'])->name('/employee');
Route::patch('/employee',[EmployeeController::class,'updateEmployee']);
Route::get('/employee/{id}',[EmployeeController::class,'editEmployee']);
Route::get('/deleteEmployee/{id}',[EmployeeController::class,'deleteEmployee']);

// doctor services
Route::get('/addDoctorServices',[DoctorServices::class,'doctorServices']);
Route::post('/doctorServices',[DoctorServices::class,'addDoctorServices']);
Route::get('/doctorServices',[DoctorServices::class,'getDoctorServices']);
Route::patch('/doctorServices',[DoctorServices::class,'updateDoctorServices']);
Route::get('/doctorServices/{id}',[DoctorServices::class,'editDoctorServices']);
Route::get('/deleteDoctorServices/{id}',[DoctorServices::class,'deleteDoctorServices']);

// doctor specialist
Route::get('/addDoctorSpecialist',[DoctorSpecialist::class,'doctorSpecialist']);
Route::post('/doctorSpecialist',[DoctorSpecialist::class,'addDoctorSpecialist']);
Route::get('/doctorSpecialist',[DoctorSpecialist::class,'getDoctorSpecialist']);
Route::patch('/doctorSpecialist',[DoctorSpecialist::class,'updateDoctorSpecialist']);
Route::get('/doctorSpecialist/{id}',[DoctorSpecialist::class,'editDoctorSpecialist']);
Route::get('/deleteDoctorSpecialist/{id}',[DoctorSpecialist::class,'deleteDoctorSpecialist']);

// Booking

Route::get('/addBooking',[Booking::class,'booking']);
Route::post('/booking',[Booking::class,'addBooking']);
Route::get('/getBooking',[Booking::class,'showBooking']);
Route::patch('/booking',[Booking::class,'updateBooking']);
Route::get('/booking/{id}',[Booking::class,'editBooking']);
Route::get('/deleteBooking/{id}',[Booking::class,'deleteBooking']);

});

